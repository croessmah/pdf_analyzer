#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QJsonArray>
#include <QJsonObject>
#include <QByteArray>
#include <QDir>
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <QIODevice>
#include <QJsonDocument>

#include "ResultSaveWindow.hpp"


namespace {
    QJsonObject getJsonObject(const ChoosedGraphData::GraphData &data, const QString &yxType)
    {
        QJsonObject currentResultObject;
        QJsonArray dataArray;
        for (auto &line: data.getLines()) {
            QJsonObject lineObject;
            QJsonArray pointArray;
#ifndef DEMO
            for (auto &point: line.getPoints()) {
                //QJsonObject pointObject;
                //pointObject["x"] = point.x();
                //pointObject["y"] = point.y();
                //pointArray.append(pointObject);
                pointArray.append(point.x());
                pointArray.append(point.y());
            }
#else
            lineObject["information:"] = QString::fromUtf8("Для демо-версии недоступно сохранение точек");
#endif
            lineObject["points"] = pointArray;
            lineObject["parameters_recommend"] = line.isRecommend();
            dataArray.append(lineObject);
        }

        currentResultObject["data_points"] = dataArray;
        currentResultObject["data_name"] = data.getName();
        currentResultObject["data_types"] = yxType;
        return currentResultObject;
    }
}


ResultSaveWindow::ResultSaveWindow(QWidget *parent) : QDialog(parent)
{
    mPumpName = new QLineEdit(tr(""));
    mSaveResultLabel = new QLabel(tr(""));
    mSave = new QPushButton(tr("Сохранить"));
    QHBoxLayout *nameLayout = new QHBoxLayout();
    nameLayout->addWidget(new QLabel(tr("Имя насоса:")));
    nameLayout->addWidget(mPumpName);
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addLayout(nameLayout);
    mainLayout->addWidget(mSave);
    mainLayout->addWidget(mSaveResultLabel);
    this->setLayout(mainLayout);

    connect(mSave, &QPushButton::clicked, this, &ResultSaveWindow::save);

}

void ResultSaveWindow::generate(const std::list<ResultData> &data)
{
    this->setModal(true);
    this->show();
    mSave->setEnabled(false);
    if (!mPumpsDataArray.isEmpty()) {
        for (auto it = mPumpsDataArray.begin(); it != mPumpsDataArray.end();) {
            it = mPumpsDataArray.erase(it);
        }
    }
    if (!mOtherDataArray.isEmpty()) {
        for (auto it = mOtherDataArray.begin(); it != mOtherDataArray.end();) {
            it = mOtherDataArray.erase(it);
        }
    }

    for (auto &item: data) {
        switch (item.mType)
        {
        case 1:
            mPumpsDataArray.append(getJsonObject(item.mData, item.mYXType));
            break;
        case 2:
            mOtherDataArray.append(getJsonObject(item.mData, item.mYXType));
            break;
        default:
            return;
        }
    }
    mSaveResultLabel->setText(tr("Генерация данных завершена"));
    mSave->setEnabled(true);
    emit generateFinished();
}



void ResultSaveWindow::save()
{
    mSaveResultLabel->setText(tr(""));
    QString pumpName = mPumpName->text();
    if (pumpName.trimmed().isEmpty()) {
        mSaveResultLabel->setText(tr("Не задано имя насоса"));
        return;
    }
    QString fileName = QFileDialog::getSaveFileName(this, tr("Выберите файл для сохранения"), QDir::home().path(), "*.json");
    if (fileName.isEmpty()) {
        mSaveResultLabel->setText(tr("Не выбран файл для сохранения"));
        return;
    }
    QFile saveFile(fileName);
    if (!saveFile.open(QIODevice::WriteOnly)) {
        mSaveResultLabel->setText(tr("Ошибка открытия файла"));
        return;
    }

    QJsonObject saveObject;
    saveObject["pump_name"] = pumpName;
    saveObject["pumps_data"] = mPumpsDataArray;
    saveObject["pumps_other_data"] = mOtherDataArray;
    QJsonDocument jsonDoc(saveObject);

    if (saveFile.write(jsonDoc.toJson(QJsonDocument::Indented)) == -1) {
        mSaveResultLabel->setText(tr("Ошибка при записи в файл"));
        return;
    }
    mSaveResultLabel->setText(tr("Данные успешно сохранены"));
    emit saveFinished();
}
