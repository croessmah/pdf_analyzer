#ifndef CHOOSEDGRAPHDATA_HPP
#define CHOOSEDGRAPHDATA_HPP

#include <QObject>
#include <QPointF>

#include "PGraphicsGraph.hpp"
#include "PGraphicsLinesStrip.hpp"
#include "PGraphicsText.hpp"

#include <list>
#include <tuple>


class QString;
class QRectF;
class PGraphicsGraph;


class LineInfo
{
public:
    LineInfo(std::vector<QPointF> &&points, bool recommend);
    LineInfo(LineInfo &&) noexcept = default;
    LineInfo(const LineInfo &) = default;
    bool isRecommend() const noexcept { return mRecommend; }
    const std::vector<QPointF> &getPoints() const noexcept { return mPoints; }
private:
    std::vector<QPointF> mPoints;
    bool mRecommend;
};

class ChoosedGraphData : public QObject
{
    Q_OBJECT
public:
    class GraphData
    {
    public:
        GraphData(PGraphicsGraph *g, std::vector<LineInfo> l);
        PGraphicsGraph *getGraphic();
        const QString &getName() const;
        void setName(const QString &newName);
        const std::vector<LineInfo> &getLines() const;
        GraphData makeCopyScaled(const QPointF &nullPoint, const QPointF &maxPoint) const;
    private:
        friend class ChoosedGraphData;
        PGraphicsGraph *graphic;
        std::vector<LineInfo> lines;
    };

    explicit ChoosedGraphData(QObject *parent = 0);
    void clear()
    {
        mGraphData.clear();
    }

    const GraphData &getData(size_t index) const {
        if (index < mGraphData.size()) {
            return mGraphData[index];
        }
        throw std::out_of_range("index not found");
    }
    GraphData &getData(size_t index) {
        if (index < mGraphData.size()) {
            return mGraphData[index];
        }
        throw std::out_of_range("index not found");
    }

    std::vector<GraphData> makeCopyAllScaled(const QPointF &nullPoint, const QPointF &maxPoint) const;

    size_t getSize() const {
        return mGraphData.size();
    }
signals:
    void loaded();    
public slots:
    void load(const QRectF &rect, std::list<PGraphicsGraph*> gr, std::list<PGraphicsText*> texts);
    void setName(const QString &name, size_t index);
    void setActivated(bool value, size_t index);
private:
    std::vector<GraphData> mGraphData;
};

#endif // CHOOSEDGRAPHDATA_HPP
