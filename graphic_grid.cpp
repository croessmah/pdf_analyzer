#include "graphic_grid.hpp"

graphic_grid::graphic_grid()
{

}

const QPoint &graphic_grid::cells_count() const noexcept
{
    return m_cells_count;
}

const QPointF &graphic_grid::cells_weight() const noexcept
{
    return m_cells_weight;
}

const QPointF &graphic_grid::zero() const noexcept
{
    return m_zero;
}

void graphic_grid::cells_count(int x, int y) noexcept
{
    m_cells_count.setX(x);
    m_cells_count.setY(y);
}

void graphic_grid::cells_weight(qreal w, qreal h) noexcept
{
    m_cells_weight.setX(w);
    m_cells_weight.setY(h);
}

void graphic_grid::zero(qreal x, qreal y) noexcept
{
    m_zero.setX(x);
    m_zero.setY(y);
}

QPointF graphic_grid::mapped_point(const QPointF &point) const noexcept
{
    return {point.x() + m_zero.x(), point.y() + m_zero.y()};
}
