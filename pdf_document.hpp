#ifndef PDF_ANALYZER_COORDINATEDOC_HPP
#define PDF_ANALYZER_COORDINATEDOC_HPP

#include "page_output_dev.hpp"

#include <memory>
#include <mutex>
//#pragma GCC diagnostic ignored "-Wunused-parameter"
//#pragma GCC diagnostic warning "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <PDFDoc.h>
#pragma GCC diagnostic warning "-Wunused-parameter"

class pdf_document
{
public:

#ifdef _WIN32
    pdf_document(wchar_t const * pdfFileName = nullptr);
#else
    pdf_document(char const * pdfFileName = nullptr);
#endif
    pdf_document(const pdf_document&) = delete;
    pdf_document &operator=(const pdf_document&) = delete;
    pdf_document(pdf_document&&) = default;
    pdf_document &operator=(pdf_document&&) = default;
    ~pdf_document() = default;

#ifdef _WIN32
    bool load(wchar_t const *pdfFileName);
#else
    bool load(char const *pdfFileName);
#endif

    bool loaded() const;
    std::shared_ptr<page_output_dev> get_page_geometry(int page, int dpi = 128);
    int pages_count() const;//[1; pages_count]
private:
    std::unique_ptr<PDFDoc> m_pdf_doc;
    mutable std::mutex m_mutex;
};

#endif // PDF_ANALYZER_COORDINATEDOC_HPP
