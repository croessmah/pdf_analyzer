#include "ChoosedGraphData.hpp"

#include <QDebug>//todo: delete
#include <QPoint>
#include <QVector2D>
#include <QRectF>
#include <QString>
#include <cmath>
#include <algorithm>


namespace
{

qreal inline dot(QPointF f, QPointF s) noexcept
{
    return f.x() * s.x() + f.y() * s.y();
}

qreal inline norm(QPointF f) noexcept
{
    return sqrt(dot(f, f));
}

qreal inline d(QPointF f, QPointF s) noexcept
{
    return norm(f - s);
}



qreal getDistanceToLine(const QLineF &l, const QPointF &p)
{
/*
    QVector2D lineVec(
        static_cast<float>(l.p1().x() - l.p2().x()),
        static_cast<float>(l.p1().y() - l.p2().y())
    );
    QVector2D pointVec(
        static_cast<float>(p.x() - l.p2().x()),
        static_cast<float>(p.y() - l.p2().y())
    );
    lineVec.normalize();
    qreal d = lineVec.distanceToPoint(pointVec);
    return d;

    qreal dY = l.p2().y() - l.p1().y();
    qreal dX = l.p2().x() - l.p1().x();
    qreal length = sqrt(dY * dY + dX * dX);

    if (length > 0) {
        return fabs(dY * p.x() - dX * p.y() + l.p2().x() * l.p1().y() - l.p2().y() * l.p1().x()) / length;
    } else {
        dY = l.p1().y() - p.y();
        dX = l.p1().x() - p.x();
        return sqrt(dY * dY + dX * dX);
    }*/
    QPointF v = l.p2() - l.p1();
    QPointF w = p - l.p1();
    qreal c1 = dot(w, v);
    if ( c1 <= 0 )
        return d(p, l.p1());
    qreal c2 = dot(v, v);
    if ( c2 <= c1 )
        return d(p, l.p2());
    qreal b = c1 / c2;
    QPointF pb = l.p1() + b * v;
    return d(p, pb);
}



PGraphicsText *findMinDistanceText(const PGraphicsGraph *gr, std::list<PGraphicsText *> texts)
{
    PGraphicsText *result = nullptr;
    qreal minDistance = std::numeric_limits<qreal>::max();
    for(auto lineStrip: *gr) {
        auto &points = lineStrip->points();
        if (points.size() < 2) {
            continue;
        }
        for(size_t i = 1; i < points.size(); ++i) {
            QPointF fPoint = points[i-1];
            QPointF sPoint = points[i];
            for (auto text: texts) {
                QRectF textRect(text->pos().x(), text->pos().y(), text->boundingRect().width(), -text->boundingRect().height());
                qreal distance = getDistanceToLine(QLineF(fPoint, sPoint), text->boundingRect().center());
                if (distance < minDistance) {
                    minDistance = distance;
                    result = text;
                }                
            }
        }
    }
    return result;
}



}



ChoosedGraphData::ChoosedGraphData(QObject *parent) : QObject(parent)
{
}



std::vector<ChoosedGraphData::GraphData> ChoosedGraphData::makeCopyAllScaled(const QPointF &nullPoint, const QPointF &maxPoint) const
{
    std::vector<GraphData> result;
    result.reserve(mGraphData.size());
    for (auto &data: mGraphData) {
        result.emplace_back(data.makeCopyScaled(nullPoint, maxPoint));
    }
    return result;
}



void ChoosedGraphData::load(const QRectF &rect, std::list<PGraphicsGraph *> graphics, std::list<PGraphicsText *> texts)
{
    qreal width = rect.width();
    qreal height = rect.height();
    qreal zeroX = rect.left();
    qreal zeroY = rect.bottom();
    //Очистить старые данные.
    mGraphData.clear();

    std::list<PGraphicsText *> findTexts = texts;

    for (PGraphicsGraph *graphic: graphics) {
        if (!graphic->getName().isEmpty()) {
            continue;
        }
        PGraphicsText *text = findMinDistanceText(graphic, findTexts);

        if (text != nullptr) {
            graphic->setName(text->getText());
            //findTexts.remove(text);
        }
    }

    for (auto graphic: graphics) {
        std::vector<LineInfo> lines;
        //Получение координат графика
        for (auto lineStrip: *graphic) {
            std::vector<QPointF> points;
            points.reserve(lineStrip->points().size());
            bool recommend = (lineStrip->type() == PGraphicsLinesStrip::LRecommend);
            for (QPointF point: lineStrip->points()) {
                //Убрать смещение относительно нулевой точки.
                point.rx() -= zeroX;
                point.ry() = zeroY - point.y();
                //Масштабирование значений до интервала [0, 1]
                point.rx() /= width;
                point.ry() /= height;
                if (point.x() >= 0 && point.y() >= 0) {
                    points.push_back(point);
                }
            }
            if (points.size() > 1) {
                lines.emplace_back(std::move(points), recommend);
            }
        }

        mGraphData.emplace_back(graphic, std::move(lines));
    }

    std::sort(
        mGraphData.begin(),
        mGraphData.end(),
        [](const GraphData &f, const GraphData &s){
            return f.getLines().front().getPoints().front().y() > s.getLines().front().getPoints().front().y();
        }
    );

    //Сгенерировать сигнал loaded
    emit loaded();
}

void ChoosedGraphData::setName(const QString &name, size_t index)
{
    getData(index).setName(name);
}

void ChoosedGraphData::setActivated(bool value, size_t index)
{
    getData(index).getGraphic()->setActive(value);
    getData(index).getGraphic()->update(getData(index).getGraphic()->boundingRect());
}



LineInfo::LineInfo(std::vector<QPointF> &&points, bool recommend)
    : mPoints(std::move(points))
    , mRecommend(recommend)
{
}

ChoosedGraphData::GraphData::GraphData(PGraphicsGraph *g, std::vector<LineInfo> l)
    : graphic(g)
    , lines(std::move(l))
{
}

PGraphicsGraph *ChoosedGraphData::GraphData::getGraphic() { return graphic; }

const QString &ChoosedGraphData::GraphData::getName() const { return graphic->getName(); }

void ChoosedGraphData::GraphData::setName(const QString &newName) { graphic->setName(newName); }

const std::vector<LineInfo> &ChoosedGraphData::GraphData::getLines() const { return lines; }

ChoosedGraphData::GraphData ChoosedGraphData::GraphData::makeCopyScaled(const QPointF &nullPoint, const QPointF &maxPoint) const
{
    qreal width = maxPoint.x() - nullPoint.x();
    qreal height = maxPoint.y() - nullPoint.y();
    std::vector<LineInfo> newLines;
    for (auto line: lines) {
        std::vector<QPointF> newPoints;
        for (auto point: line.getPoints()) {
            QPointF p(point.x() * width + nullPoint.x(), point.y() * height + nullPoint.y());
            newPoints.push_back(p);
        }
        newLines.emplace_back(std::move(newPoints), line.isRecommend());
    }
    return GraphData(graphic, std::move(newLines));
}
