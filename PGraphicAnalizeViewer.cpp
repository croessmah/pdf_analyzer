#include <QWidget>
#include <QGraphicsScene>
#include <QResizeEvent>
#include <QLineF>
#include <QtMath>
#include <QGraphicsLineItem>
#include <QGraphicsRectItem>
#include <QList>
#include <QDebug>//todo: delete
#include <QMouseEvent>
#include <QFont>
#include <QFontMetrics>

#include <cmath>
#include <algorithm>
#include <numeric>
#include <iostream> //todo: delete this
#include <tuple> //todo: delete this
#include <vector> //todo: delete this

#include "optional.hpp"
#include "PGraphicAnalizeViewer.hpp"
#include "PGraphicsLinesStrip.hpp"
#include "PGraphicsText.hpp"
#include "PGraphicsLinesStrip.hpp"
#include "GlobalSettings.hpp"
#include "line_segment.hpp"
#include "real_tools.hpp"
#include "graphics_box.hpp"
#include "graphics_box_builder.hpp"

namespace {



qreal getSqrLineLength(const QPointF &p1, const QPointF &p2)
{
    qreal dx = p1.x() - p2.x();
    qreal dy = p1.y() - p2.y();
    return dx * dx + dy * dy;
}


}//internal linkage



PGraphicAnalizeViewer::PGraphicAnalizeViewer(QWidget *parent)
    : QGraphicsView(parent)
    , mLineVis(nullptr)
    , mRectVis(nullptr)
    , mState(State::StartState)
{
    setAlignment(Qt::AlignLeft | Qt::AlignTop);
    mScene = new QGraphicsScene(this);
    setScene(mScene);
    setMouseTracking(true);
    stateStartMode();
}



#include <QPointF>
#include <QLineF>
#include <vector>
#include <algorithm>
#include <memory>
#include <utility>
#include <stack>
#include <thread>

#include <QDebug>


PGraphicsLinesStrip *makeGraphicsLinesStripFromPointPath(const points_path &path)
{
    PGraphicsLinesStrip *graphicsLine = new PGraphicsLinesStrip();
    for (auto &p: path) {
        graphicsLine->addPoint(p);
    }
    return graphicsLine;
}


std::ostream &operator<<(std::ostream &stream, const line_segment &segment)
{
    if (segment.is_horizontal(0.000001)) {
        stream << "horizontal ";
    } else if (segment.is_vertical(0.000001)){
        stream << "vertical ";
    } else {
        stream << "other ";
    }

    stream << "segment ["
           << segment.line().p1().x() << ", " << segment.line().p1().y() << "; "
           << segment.line().p2().x() << ", " << segment.line().p2().y() << "] ->"
           << segment.width() << "<-";
    return stream;
}





bool PGraphicAnalizeViewer::setCoordinatePage(const CoordinatePage &cp)//todo: передача настроек
{
    std::list<points_path> pathes = cp.getPathesList();
    std::vector<qreal> otherLinesWidthes;
    mScene->clear();
    mGraphics.clear();
    mOtherLines.clear();
    mTexts.clear();
    mState = State::StartState;
    //fitInView(mScene->sceneRect(), Qt::AspectRatioMode::IgnoreAspectRatio);

    graphics_box_builder gbb;
    gbb.rebuild(pathes, 0.000001, 0.000001, 20, std::cout);

    GlobalSettings &settings = gSettings();
    
    double rlineWidth = settings.getRecommendLineWidth();
    double nrlineWidth = settings.getNonRecommendLineWidth();
    double reps = settings.getRecommendLineWidthEps();
    double nreps = settings.getNonRecommendLineWidthEps();
    double peps = settings.getPointEps();


    //Ищем все рекомендуемые графики
    for (auto begin = pathes.begin(), end = pathes.end(); begin != end;) {
        //Если линия пустая
        if (begin->empty()) {
            begin = pathes.erase(begin);//Удаляем путь из списка
            continue;
        }
        //Если не рекомендуемая (по ширине)
        if (
            !qreal_compare(begin->width(), rlineWidth, reps)
        ) {
            ++begin;//пропускаем её
        } else {//иначе
            //Создаем графическую линию и добавляем её в список графиков
            PGraphicsGraph *graphic = new PGraphicsGraph();
            PGraphicsLinesStrip *graphicLine = makeGraphicsLinesStripFromPointPath(*begin);
            graphicLine->setType(PGraphicsLinesStrip::LRecommend);
            graphic->addLineBack(graphicLine);
            mGraphics.push_back(graphic);
            begin = pathes.erase(begin);//Удаляем путь из списка
        }
    }

    std::list<std::pair<points_path, PGraphicsLinesStrip*>> olines;


    for (auto begin = pathes.begin(), end = pathes.end(); begin != end; ++begin) {
        //Определяем является линия не рекомендуемой или любой другой
        //Рекомендуемая линия определяется тем, что её толщина равна заданной
        //и один из её концов совпадает с одним из концов рекомендуемой линии
        QPointF p1(begin->front());
        QPointF p2(begin->back());

        bool widthAsNRecommend =  qreal_compare(begin->width(), nrlineWidth, nreps);

        PGraphicsLinesStrip *graphicLine = makeGraphicsLinesStripFromPointPath(*begin);
        bool isAdded = false;
        for (auto graphic: mGraphics) {
           QPointF rp1 = graphic->front()->points().front();
           QPointF rp2 = graphic->back()->points().back();
           //Если конец линии совпадает с началом графика
           if (widthAsNRecommend && point_compare(p2, rp1, peps)) {
               isAdded = true;
               graphic->addLineFront(graphicLine);
               graphicLine->setType(PGraphicsLinesStrip::LNoRecommend);
               break;
           }
           //Если начало линии совпадает с концом графика
           if (widthAsNRecommend && point_compare(p1, rp2, peps)) {
               isAdded = true;
               graphic->addLineBack(graphicLine);
               graphicLine->setType(PGraphicsLinesStrip::LNoRecommend);
               break;
           }
        }
        if (!isAdded) {
            olines.emplace_back(*begin, graphicLine);
            //mOtherLines.push_back(graphicLine);
        }
    }

    bool isAdded = false;
    do {
        for (auto begin = olines.begin(), end = olines.end(); begin != end;) {
            QPointF p1(begin->first.front());
            QPointF p2(begin->first.back());


            bool widthAsNRecommend =  qreal_compare(begin->first.width(), nrlineWidth, nreps);

            PGraphicsLinesStrip *graphicLine = begin->second;
            isAdded = false;
            for (auto graphic: mGraphics) {
               QPointF rp1 = graphic->front()->points().front();
               QPointF rp2 = graphic->back()->points().back();
               //Если конец линии совпадает с началом графика
               if (widthAsNRecommend && point_compare(p2, rp1, peps)) {
                   isAdded = true;
                   graphic->addLineFront(graphicLine);
                   graphicLine->setType(PGraphicsLinesStrip::LNoRecommend);
                   break;
               }
               //Если начало линии совпадает с концом графика
               if (widthAsNRecommend && point_compare(p1, rp2, peps)) {
                   isAdded = true;
                   graphic->addLineBack(graphicLine);
                   graphicLine->setType(PGraphicsLinesStrip::LNoRecommend);
                   break;
               }
            }
            if (isAdded) {
                begin = olines.erase(begin);
            } else {
                ++begin;
            }
        }
    }while(isAdded);

    for (auto &p: olines) {
        mOtherLines.push_back(p.second);
    }


    //todo: добавить объединение графиков
    QRectF clipRect;
    clipRect.setLeft(settings.getPageClipLeft());
    clipRect.setRight(settings.getPageClipRight());
    clipRect.setTop(settings.getPageClipTop());
    clipRect.setBottom(settings.getPageClipBottom());

    for (auto &graphic: mOtherLines) {
        QRectF lineRect(graphic->boundingRect());
        if (
            !clipRect.contains(lineRect)
        ) {
            delete graphic;
            graphic = nullptr;
        } else {
            mScene->addItem(graphic);
        }
    }

    for (auto &graphic: mGraphics) {
        QRectF lineRect(graphic->boundingRect());
        if (
            !clipRect.contains(lineRect)
        ) {
            delete graphic;
            graphic = nullptr;
        } else {
            mScene->addItem(graphic);
        }
    }
    mOtherLines.remove(nullptr);
    mGraphics.remove(nullptr);

    for (auto &e: cp.getWordsData()){
        if (e.word.isEmpty()) {
            continue;
        }
        QFont font;
        font.setFamily(e.fontName);
        font.setBold(e.bold);
        font.setItalic(e.italic);
        font.setPointSizeF(e.fontSize * 0.72);

        PGraphicsText *simpleText = new PGraphicsText;
        simpleText->setPos(QPointF(e.cCoord.x(), e.cCoord.y()));
        simpleText->setFont(font);
        simpleText->setText(e.word);
        /*
        qDebug() << e.word;
        qDebug() << "Yeah!";
        qDebug() << "c:" << e.cCoord.getX() << "x" << e.cCoord.getY();
        qDebug() << "d:" << e.dCoord.getX() << "x" << e.dCoord.getY();
        qDebug() << "o:" << e.oCoord.getX() << "x" << e.oCoord.getY();
        */
        QRectF textRect(e.cCoord.x(), e.cCoord.y(), simpleText->boundingRect().width(), simpleText->boundingRect().height());
        if (
            !clipRect.contains(textRect)
        ) {
            delete simpleText;
        } else {
            mScene->addItem(simpleText);
        }
    }
    QRectF rect(clipRect);
    mScene->setSceneRect(clipRect);

    mLineVis = new QGraphicsLineItem();
    mRectVis = new QGraphicsRectItem();
    QPen pen;
    pen.setWidth(3);
    pen.setColor(Qt::blue);
    mRectVis->setPen(pen);
    mRectVis->setRect(rect.center().x(), rect.center().y(), 0, 0);
    mScene->addItem(mRectVis);
    mLineVis->setPen(pen);
    mLineVis->setLine(rect.center().x(), rect.center().y(), 0, 0);
    mScene->addItem(mLineVis);


    //fitInView(mScene->sceneRect(), Qt::AspectRatioMode::KeepAspectRatio);
    stateStartMode();
    return true;
}


void PGraphicAnalizeViewer::addGraphLines(PGraphicsGraph *graph)
{
    mScene->addItem(graph);
    //fitInView(mScene->sceneRect(), Qt::AspectRatioMode::KeepAspectRatio);
}

bool PGraphicAnalizeViewer::isAllHidden() const
{
    for (auto g: mGraphics) {
        if (g->isVisible()) {
            return false;
        }
    }
    return true;
}

void PGraphicAnalizeViewer::stateAreaMode()
{
    mState = State::FindFirstPoint;
    mCurrentPoint.clear();
    mFirstPoint.clear();
    mSecondPoint.clear();
    if (mRectVis) {
        mRectVis->hide();
    }
    if (mLineVis) {
        mLineVis->hide();
    }
    mScene->update(mScene->sceneRect());
}

void PGraphicAnalizeViewer::stateStartMode()
{
    mState = State::StartState;
    if (mRectVis) {
        mRectVis->hide();
    }
    if (mLineVis) {
        mLineVis->hide();
    }
    for (auto gr: mGraphics) {
        gr->setChoosed(false);
    }
    mScene->update(mScene->sceneRect());
}



void PGraphicAnalizeViewer::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);
    //fitInView(mScene->sceneRect(), Qt::AspectRatioMode::KeepAspectRatio);
}




void PGraphicAnalizeViewer::mouseMoveEvent(QMouseEvent *event)
{
    switch(mState)
    {
    case State::StartState:
        break;
    case State::FindFirstPoint:
    {
        QPointF point = event->localPos();
        QRectF rect = mScene->sceneRect();
        point = this->mapToScene({static_cast<int>(point.x()), static_cast<int>(point.y())});
        mCurrentPoint = findMinDistancePointFromRect(point, rect);
        if (mCurrentPoint.isSet()) {            
            if (mLineVis) {
                mLineVis->setLine({point, *mCurrentPoint.get()});
                mLineVis->show();
            }
        } else {
            if (mLineVis) {
                mLineVis->hide();
            }
        }
        break;
    }
    case State::FindSecondPoint:
    {
        if (mLineVis) {
            mLineVis->hide();
        }
        QPointF point = event->localPos();
        QRectF rect = mScene->sceneRect();
        point = this->mapToScene({static_cast<int>(point.x()), static_cast<int>(point.y())});
        mCurrentPoint = findMinDistancePointFromRect(point, rect);
        if (mCurrentPoint.isSet()) {            
            if (mRectVis) {

                QPointF topLeft;
                QPointF bottomRight;

                topLeft.setX(std::min(mFirstPoint.get()->x(), mCurrentPoint.get()->x()));
                topLeft.setY(std::min(mFirstPoint.get()->y(), mCurrentPoint.get()->y()));
                bottomRight.setX(std::max(mFirstPoint.get()->x(), mCurrentPoint.get()->x()));
                bottomRight.setY(std::max(mFirstPoint.get()->y(), mCurrentPoint.get()->y()));



                mRectVis->setRect({topLeft, bottomRight});
                mRectVis->update();
                mRectVis->show();
            }
        } else {
            if (mRectVis) {
                mRectVis->hide();
            }
        }
        break;
    }
    case State::GraphicsAnalizing:
        break;
    }

}

void PGraphicAnalizeViewer::mousePressEvent(QMouseEvent *event)
{
    switch(mState)
    {
    case State::StartState:
        if (event->button() == Qt::MouseButton::LeftButton) {
            stateAreaMode();
        }
        break;
    case State::FindFirstPoint:
    {
        if (event->button() == Qt::MouseButton::RightButton) {
            stateStartMode();
        } else {
            if (mCurrentPoint.isSet()) {
                mFirstPoint = mCurrentPoint;
                mState = State::FindSecondPoint;
            }
        }
        break;
    }
    case State::FindSecondPoint:
        if (event->button() == Qt::MouseButton::RightButton) {
            stateStartMode();
        } else {
            if (mCurrentPoint.isSet()) {
                mSecondPoint = mCurrentPoint;
                mState = State::GraphicsAnalizing;

                analizeAndEmit();

                mScene->update(mScene->sceneRect());
            }
        }
        break;
    case State::GraphicsAnalizing:
        if (event->button() == Qt::MouseButton::RightButton) {
            stateStartMode();
        }
        break;
    }
}



void PGraphicAnalizeViewer::analizeAndEmit()
{
    if (!(mFirstPoint.isSet() && mSecondPoint.isSet())) {
        return;
    }

    const QPoint &fpr = mFirstPoint.get()->toPoint();
    const QPoint &spr = mSecondPoint.get()->toPoint();
    QPointF firstPoint;
    QPointF secondPoint;

    firstPoint.setX(std::min(fpr.x(), spr.x()));
    firstPoint.setY(std::min(fpr.y(), spr.y()));
    secondPoint.setX(std::max(fpr.x(), spr.x()));
    secondPoint.setY(std::max(fpr.y(), spr.y()));

    QRectF findRect;
    findRect.setCoords(firstPoint.x() - 10, firstPoint.y() - 10, secondPoint.x() + 10, secondPoint.y() + 10);

    std::list<PGraphicsGraph*> graphics;
    std::list<PGraphicsText*> texts;


    QList<QGraphicsItem*> items = mScene->items(findRect, Qt::ContainsItemBoundingRect);

    for (auto item: items) {
        switch (item->type())
        {
        case PGraphicsGraph::GGraph:
        {
            PGraphicsGraph *graphItem = static_cast<PGraphicsGraph*>(item);
            if (graphItem->isVisible()) {
                graphics.push_back(graphItem);
            }
            break;
        }
        case PGraphicsText::TText:
        {
            PGraphicsText *textItem = static_cast<PGraphicsText*>(item);
            if (textItem->isVisible()) {
                texts.push_back(textItem);
            }
            break;
        }
        default:
            break;
        }
    }

    QRectF emitRect;
    emitRect.setCoords(firstPoint.x(), firstPoint.y(), secondPoint.x(), secondPoint.y());

    emit rectChoosed(emitRect, graphics, texts);
}



optional<QPointF> PGraphicAnalizeViewer::findMinDistancePoint(const QPointF &point)
{
    optional<QPointF> minDistancePoint;

    qreal minDistance = std::numeric_limits<double>::max();


    for (auto linePtr: mOtherLines) {
        if (getSqrLineLength(linePtr->points().front(), linePtr->points().back()) < 10*10) {
            continue;
        }
        qreal distance = getSqrLineLength(linePtr->points().front(), point);
        if (distance < minDistance) {
            minDistance = distance;
            minDistancePoint.reset(linePtr->points().front());
        }
        distance = getSqrLineLength(linePtr->points().back(), point);
        if (distance < minDistance) {
            minDistance = distance;
            minDistancePoint.reset(linePtr->points().back());
        }
    }
    return minDistancePoint;
}

optional<QPointF> PGraphicAnalizeViewer::findMinDistancePointFromRect(const QPointF &point, const QRectF &rect)
{
    if (point.x() < rect.left() + 10
        || point.x() > rect.right() - 10
        || point.y() < rect.top() + 10
        || point.y() > rect.bottom() - 10
    ) {
        return {};
    }
    return findMinDistancePoint(point);
}
