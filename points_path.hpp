#ifndef POINTS_PATH_HPP
#define POINTS_PATH_HPP

#include <QPointF>
#include "line_segment.hpp"
#include <vector>




class points_path
{
public:
    using container_t = std::vector<QPointF>;
    using iterator_t = container_t::iterator;
    using const_iterator_t = container_t::const_iterator;
    points_path();
    points_path(const points_path&) = default;
    points_path(points_path &&) noexcept = default;
    points_path &operator=(const points_path &) = default;
    points_path &operator=(points_path &&) noexcept = default;
    void push_back(const QPointF &point);
    const QPointF &operator[](size_t index) const noexcept;
    QPointF &operator[](size_t index) noexcept;
    const QPointF &front() const noexcept;
    const QPointF &back() const noexcept;
    size_t size() const noexcept;
    bool empty() const noexcept;
    void width(qreal w) noexcept;
    qreal width() const noexcept;
    int style() const noexcept;
    void style(int s) noexcept;
    void clear() noexcept;
    iterator_t begin() noexcept;
    iterator_t end() noexcept;
    const_iterator_t begin() const noexcept;
    const_iterator_t end() const noexcept;
    void reserve(size_t cap);
private:
    container_t m_points;
    int m_style;
    qreal m_width;
};

#endif // POINTS_PATH_HPP
