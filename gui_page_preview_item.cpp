#include "gui_page_preview_item.hpp"

#include <QListWidget>
#include <QPixmap>
#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <memory>
#include <QFrame>


gui_page_preview_item::gui_page_preview_item(int page, const QPixmap &preview, easytl::not_null_pointer<QListWidget> parent):
    QListWidgetItem(parent),
    m_page((page > 0)? page: -1)
{
    std::unique_ptr<QVBoxLayout> layout(std::make_unique<QVBoxLayout>());
    std::unique_ptr<QLabel> page_label(std::make_unique<QLabel>(QString::number(page)));
    std::unique_ptr<QLabel> preview_label(std::make_unique<QLabel>());
    std::unique_ptr<QWidget> wgt(std::make_unique<QWidget>());
    preview_label->setPixmap(preview.scaled(150, 211));
    preview_label->setStyleSheet("border: 2px solid black; padding: 2px");
    layout->addWidget(preview_label.get());
    layout->addWidget(page_label.get());
    layout->setAlignment(preview_label.get(), Qt::AlignCenter);
    layout->setAlignment(page_label.get(), Qt::AlignCenter);    
    wgt->setLayout(layout.get());
    this->setSizeHint(wgt->sizeHint());
    parent->setItemWidget(this, wgt.get());
    wgt.release();
    layout.release();
    page_label.release();
    preview_label.release();
}



int gui_page_preview_item::page() const noexcept
{
    return m_page;
}
