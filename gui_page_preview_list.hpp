#ifndef GUI_PAGE_PREVIEW_LIST_HPP
#define GUI_PAGE_PREVIEW_LIST_HPP

#include <QListWidget>
#include <QPixmap>

QT_FORWARD_DECLARE_CLASS(QListWidgetItem);
QT_FORWARD_DECLARE_CLASS(QPixmap);

class gui_page_preview_list : public QListWidget
{
    Q_OBJECT
public:
    gui_page_preview_list(QWidget *parent = nullptr);
public slots:
    void selectItem(QListWidgetItem *, QListWidgetItem *);
    void create_new_item(int page, QPixmap pixmap);
signals:
    void page_changed(int page);
};

#endif // GUI_PAGE_PREVIEW_LIST_HPP
