#include "gui_arrow_button.hpp"
#include "gui_global_resource.hpp"

#include <QPalette>
#include <QBrush>
#include <QPixmap>
#include <QMouseEvent>

gui_arrow_button::gui_arrow_button(directions dir, QWidget *parent) :
    QWidget(parent),
    m_brush_forward(nullptr),
    m_brush_reverse(nullptr),
    m_state(false),
    m_last_mouse_left_press(false)
{    
    m_brush_forward = new QBrush();
    m_brush_reverse = new QBrush();
    m_brush_reverse->texture().size();
    set_direction(dir);
}

gui_arrow_button::~gui_arrow_button()
{
    destroy_resources();
}



void gui_arrow_button::set_direction(gui_arrow_button::directions dir)
{
    QPixmap *pixmap_forward = nullptr;
    QPixmap *pixmap_reverse = nullptr;
    bool horizontal = true;
    switch (dir)
    {
    case directions::ToRight:
        pixmap_forward = get_right_arrow();
        pixmap_reverse = get_left_arrow();
        break;
    case directions::ToLeft:
        pixmap_forward = get_left_arrow();
        pixmap_reverse = get_right_arrow();
        break;
    case directions::ToTop:
        horizontal = false;
        break;
    case directions::ToBottom:
        horizontal = false;
        break;
    };

    if(!horizontal) {
        throw std::runtime_error(u8"Vertical directions not supported");
    }

    m_brush_forward->setTexture(*pixmap_forward);
    m_brush_reverse->setTexture(*pixmap_reverse);
    update_palette();
}

void gui_arrow_button::mousePressEvent(QMouseEvent *event)
{
    m_last_mouse_left_press = event->button() == Qt::LeftButton;
}

void gui_arrow_button::mouseMoveEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
    m_last_mouse_left_press = false;
}

bool gui_arrow_button::get_state() const noexcept
{
    return m_state;
}

QSize gui_arrow_button::get_size() const noexcept
{
    return m_size;
}

void gui_arrow_button::toggle()
{
    m_state = !m_state;
    update_palette();
    emit toggled(m_state);
}

void gui_arrow_button::update_palette()
{
    QPalette palette;
    QBrush *primary = m_state? m_brush_forward: m_brush_reverse;
    palette.setBrush(this->backgroundRole(), *primary);
    m_size = primary->texture().size();
    this->setAutoFillBackground(true);
    this->setPalette(palette);
}

void gui_arrow_button::destroy_resources() noexcept
{
    delete m_brush_forward;
    delete m_brush_reverse;
    m_brush_forward = nullptr;
    m_brush_reverse = nullptr;
}

void gui_arrow_button::mouseReleaseEvent(QMouseEvent *event)
{
    if (m_last_mouse_left_press && event->button() == Qt::LeftButton) {
        toggle();
    }
    m_last_mouse_left_press = false;
}
