#ifndef PAGE_PAINTER_HPP
#define PAGE_PAINTER_HPP

#include <QPixmap>
#include <memory>

QT_FORWARD_DECLARE_CLASS(page_output_dev)

class page_painter
{
public:
    page_painter();
    page_painter(page_output_dev &dev);
    void paint(page_output_dev &dev);
    const QPixmap &pixmap() const noexcept;
private:
    std::unique_ptr<QPixmap> m_pixmap;
};

#endif // PDF_PAGE_PAINTER_HPP
