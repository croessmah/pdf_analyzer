#include "gui_page_analize_viewer.hpp"
#include "page_painter.hpp"
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QVBoxLayout>
#include <QPen>
#include <QPixmap>

#include <QDebug>
gui_page_analize_viewer::gui_page_analize_viewer(QWidget *parent) : QWidget(parent)
{
    m_scene = new QGraphicsScene(this);
    m_view = new QGraphicsView(m_scene, this);
    QVBoxLayout *main_layout = new QVBoxLayout;
    main_layout->addWidget(m_view);
    setLayout(main_layout);
}

void gui_page_analize_viewer::load_geometry(std::shared_ptr<pdf_document> doc, int page)
{
    if (doc == nullptr) {
        m_scene->clear();
        return;//todo: clear states
    }
    qDebug() << "load geometry";
    page_painter painter;
    painter.paint(*doc->get_page_geometry(page, 128));//todo: dpi
    m_scene->clear();
    m_scene->addPixmap(painter.pixmap());
    //m_scene->setSceneRect(page_device->get_page_rect());
    m_scene->update(m_scene->sceneRect());
    m_view->resetTransform();
    m_view->resetMatrix();
    //m_view->updateSceneRect(m_scene->sceneRect());
}
