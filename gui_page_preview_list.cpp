#include "gui_page_preview_list.hpp"
#include "gui_page_preview_item.hpp"

gui_page_preview_list::gui_page_preview_list(QWidget *parent):
    QListWidget (parent)
{
    this->setSpacing(5);
    connect(this, &gui_page_preview_list::currentItemChanged, this, &gui_page_preview_list::selectItem);
}

void gui_page_preview_list::selectItem(QListWidgetItem *c, QListWidgetItem *p)
{
    gui_page_preview_item *next = static_cast<gui_page_preview_item*>(c);
    if (next != nullptr) {
        emit page_changed(next->page());
    }
}

void gui_page_preview_list::create_new_item(int page, QPixmap pixmap)
{
    new gui_page_preview_item(page, pixmap, this);
}
