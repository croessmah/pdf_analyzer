#include "gui_document_previewer.hpp"
#include "gui_collapse_widget.hpp"
#include "gui_page_preview_list.hpp"
#include "gui_page_preview_item.hpp"
#include "document_loader.hpp"

#include <QFileDialog>
#include <QString>
#include <QThread>
#include <QVBoxLayout>
#include <QPushButton>

#include <QDebug>

gui_document_previewer::gui_document_previewer(QWidget *parent):
    QWidget(parent),
    m_load_thread(nullptr)
{
    QWidget *wgt = new QWidget(this);
    QPushButton *button = new QPushButton(tr("Загрузка PDF"));
    QVBoxLayout *layout = new QVBoxLayout();
    m_preview_list = new gui_page_preview_list();
    layout->addWidget(button);
    layout->addWidget(m_preview_list);
    wgt->setLayout(layout);
    m_collapse_widget = new gui_collapse_widget(gui_collapse_widget::directions::RightToLeft, wgt);
    layout->setMargin(0);
    layout->setSpacing(0);
    connect(button, &QPushButton::clicked, this, &gui_document_previewer::choose_document);
    QVBoxLayout *main_layout = new QVBoxLayout();
    main_layout->addWidget(m_collapse_widget);
    main_layout->setMargin(0);
    main_layout->setSpacing(0);
    setLayout(main_layout);
    connect(m_preview_list, &gui_page_preview_list::page_changed, this, &gui_document_previewer::choose_page, Qt::QueuedConnection);
}

void gui_document_previewer::choose_page(int page)
{
    emit page_choosed(m_doc, page);
}

void gui_document_previewer::choose_document()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Выберите PDF документ"), QDir::home().path(), "*.pdf");
    if (fileName.isEmpty()) {
        return;
    }
    std::unique_ptr<document_loader> new_loader(new document_loader());
    new_loader->load(fileName);
    std::shared_ptr<pdf_document> doc = new_loader->document();
    if (!(doc && doc->loaded())) {
        emit document_load_failed();
        return;
    }
    if (m_load_thread != nullptr) {
        m_loader->stop_load_preview();
        m_load_thread->quit();
        m_load_thread->wait(3000);
        if (m_load_thread->isRunning()) {
            m_load_thread->terminate();
        }
    }
    m_loader = std::move(new_loader);
    clear_preview();
    m_load_thread = new QThread();
    connect(m_load_thread, SIGNAL(finished()), m_load_thread, SLOT(deleteLater()));
    connect(m_load_thread, SIGNAL(started()), m_loader.get(), SLOT(create_pages_geometry()), Qt::DirectConnection);
    connect(m_loader.get(), SIGNAL(page_loaded(int, QPixmap)), m_preview_list, SLOT(create_new_item(int, QPixmap)));
    m_doc = std::move(doc);
    m_load_thread->start();
}

void gui_document_previewer::clear_preview()
{
    m_preview_list->clear();
}
