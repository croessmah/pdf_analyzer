#include "CoordinatePage.hpp"
#include <page_output_dev.hpp>
#include <PDFDoc.h>
#include <Page.h>

#include "GlobalSettings.hpp"

#include <iostream>
#include <cmath>
#include <algorithm>
#include "real_tools.hpp"


#include <QPainter>
#include <QImage>
#include <QPixmap>
#include <QPen>
#include <QDebug>
#include "SplashOutputDev.h"
#include "SplashBitmap.h"

CoordinatePage::CoordinatePage(PDFDoc *pdfDoc, int page)
{
    if (page < 1 || page > pdfDoc->getNumPages()) {
        throw std::invalid_argument("Invalid page number");
    }
    GlobalSettings &settings = gSettings();
    mOutputDev.reset(
        new page_output_dev()
    );
    pdfDoc->displayPage(mOutputDev.get(), page, 128, 128, 0, gTrue, gTrue, gFalse);

    std::vector<double> widthes;
    for (auto &path: mOutputDev->getPathList()) {
        double lineWidth = path.width();
        bool found = false;
        for (auto w: widthes) {
            if (qreal_compare(lineWidth, w, 0.00001)) {
                found = true;
                break;
            }
        }
        if (!found) {
            widthes.push_back(lineWidth);
        }
    }
    mPageNum = page;
    settings.setPageClipLeft(mOutputDev.get()->get_page_rect().left());
    settings.setPageClipTop(mOutputDev.get()->get_page_rect().top());
    settings.setPageClipRight(mOutputDev.get()->get_page_rect().right());
    settings.setPageClipBottom(mOutputDev.get()->get_page_rect().bottom());
    settings.setFoundedWidth(widthes);
    mCropLeft = mOutputDev.get()->get_page_rect().left();
    mCropRight = mOutputDev.get()->get_page_rect().right();
    mCropTop = mOutputDev.get()->get_page_rect().top();
    mCropBottom = mOutputDev.get()->get_page_rect().bottom();
    mWidth = mCropRight - mCropLeft;
    mHeight = mCropBottom - mCropTop;

    SplashColor paperColor;
    paperColor[0] = paperColor[1] = paperColor[2] = 0xFF;
    SplashOutputDev *dev = new SplashOutputDev(SplashColorMode::splashModeRGB8, 1, gFalse, paperColor);
    dev->setSkipText(gTrue, gTrue);
    dev->startDoc(pdfDoc->getXRef());
    pdfDoc->displayPage(dev, page, 128, 128, 0, gTrue, gTrue, gFalse);
    QImage img (dev->getBitmap()->getDataPtr(), dev->getBitmapWidth(), dev->getBitmapHeight(), dev->getBitmap()->getRowSize(), QImage::Format_RGB888);
}

double CoordinatePage::getPageWidth() const
{
    return mWidth;
}

double CoordinatePage::getPageHeight() const
{
    return mHeight;
}


const std::list<CoordinatePage::WordData> &CoordinatePage::getWordsData() const
{
    return mOutputDev->getWordsData();
}

const std::list<points_path> &CoordinatePage::getPathesList() const
{
    return mOutputDev->getPathList();
}

int CoordinatePage::getPageNum() const
{
    return mPageNum;
}
