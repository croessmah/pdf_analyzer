#ifndef GUI_COLLAPSE_WIDGET_HPP
#define GUI_COLLAPSE_WIDGET_HPP

#include <QWidget>
#include <QtGlobal>

#include "gui_arrow_button.hpp"

QT_FORWARD_DECLARE_CLASS(QImage);
QT_FORWARD_DECLARE_CLASS(QPushButton);

//todo: delete
QT_FORWARD_DECLARE_CLASS(QLabel);

class gui_collapse_widget : public QWidget
{
    Q_OBJECT
public:
    enum class directions: unsigned int
    {
        LeftToRight,
        RightToLeft
    };
    explicit gui_collapse_widget(directions dir, QWidget *some_widget, QWidget *parent = 0);
    QWidget *get_subwidget() const noexcept;
signals:
    void collapse(bool value);
public slots:

private slots:
    void hide_panel(bool value);
private:
    void setup(gui_arrow_button::directions dir, QWidget *some_widget);
    directions m_dir;
    gui_arrow_button *m_hide_button;
    QWidget *m_some_widget;
};

#endif // GUI_COLLAPSE_WIDGET_HPP
