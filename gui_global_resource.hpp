#ifndef GUI_GLOBAL_RESOURCE_HPP
#define GUI_GLOBAL_RESOURCE_HPP



#include <QtGlobal>


QT_FORWARD_DECLARE_CLASS(QPixmap);
QT_FORWARD_DECLARE_CLASS(QString);

const QString &get_gres_path();

QPixmap *get_right_arrow() noexcept;

QPixmap *get_left_arrow() noexcept;

#endif // GUI_GLOBAL_RESOURCE_HPP
