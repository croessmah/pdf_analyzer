#include "page_output_dev.hpp"

#include <GfxState.h>
#include <Gfx.h>
#include <GfxFont.h>
#include <UnicodeMap.h>
#include <CharTypes.h>

#include <iostream>
#include "GlobalSettings.hpp"


#include <QDebug>//todo: delete

#include "real_tools.hpp"


inline void point_transform_with_ctm(QPointF &point, double *ctm) noexcept
{
    point.rx() = ctm[0] * point.x() + ctm[2] * point.y() + ctm[4];
    point.ry() = ctm[1] * point.x() + ctm[3] * point.y() + ctm[5];
}


page_output_dev::page_output_dev()
   : m_curve_resolution(10)
{
}


void page_output_dev::stroke(GfxState *state)
{
    if (!qreal_compare(state->getStrokeOpacity(), 1.0, 0.1) || state->getBlendMode() != gfxBlendNormal) {
       return;
    }
    if (!state->getStrokeAdjust()) {
        return;
    }
    double lineWidth = state->getLineWidth();
    //int style = state->getLineCap()

    double *ctm = state->getCTM();
    GfxPath *path = state->getPath();
    clip(state);

    if (state->getRender() == 3) {
        return;
    }

    for (int i = 0, subCount = path->getNumSubpaths(); i < subCount; ++i) {
        GfxSubpath *subPath = path->getSubpath(i);
        if (subPath->getNumPoints() < 2) {//Если подпуть - точка, то пропускаем его
            continue;
        }

        m_pathes.push_back({});
        m_pathes.back().reserve(subPath->getNumPoints());
        m_pathes.back().width(lineWidth);
        //todo: определить стиль линии


        QPointF point(subPath->getX(0), subPath->getY(0));
        point_transform_with_ctm(point, ctm);
        moveTo(point);
        //Добавляем все точки пути
        for (int j = 1, pointsCount = subPath->getNumPoints(); j < pointsCount;) {
            if (subPath->getCurve(j)) {
                std::array<QPointF, 3> points {{
                    {QPointF(subPath->getX(j), subPath->getY(j))},
                    {QPointF(subPath->getX(j + 1), subPath->getY(j + 1))},
                    {QPointF(subPath->getX(j + 2), subPath->getY(j + 2))}
                }};

                for (QPointF &p: points) {
                    point_transform_with_ctm(p, ctm);
                }
                curveTo(points);
                j += points.size();
            } else {
                point.setX(subPath->getX(j));
                point.setY(subPath->getY(j));
                point_transform_with_ctm(point, ctm);
                lineTo(point);
                ++j;
            }
        }
        //Если путь закрыт, добавляем первую точку в конец
        if (subPath->isClosed() && !m_pathes.back().empty()) {
            m_pathes.back().push_back(m_pathes.back()[0]);
        }
        if (m_pathes.back().empty()) {
            m_pathes.pop_back();
        }
    }
}

void page_output_dev::startPage(int pageNum, GfxState *state)
{
    (void)pageNum;
    clip(state);
}



const std::list<points_path> &page_output_dev::pathes() const noexcept
{
    return m_pathes;
}



const std::list<page_output_dev::word> &page_output_dev::words() const noexcept
{
    return m_words;
}

const QRectF &page_output_dev::get_page_rect() const noexcept
{
    return m_page_rect;
}



GBool page_output_dev::upsideDown()
{
    return gTrue;
}



GBool page_output_dev::useDrawChar()
{
    return gFalse;
}



GBool page_output_dev::interpretType3Chars()
{
    return gTrue;
}

GBool page_output_dev::needCharCount()
{
    return gTrue;
}

void page_output_dev::incCharCount(int nChars)
{
    m_skip_char_count += nChars;
}



void page_output_dev::drawChar(GfxState *state, double x, double y, double dx, double dy, double originX, double originY, CharCode code, int nBytes, Unicode *u, int uLen)
{


    if (!qreal_compare(state->getStrokeOpacity(), 1.0, 0.1) || state->getBlendMode() != gfxBlendNormal) {
       return;
    }

    if (state->getFillOverprint() || state->getRender() == 3 || state->getStrokeColorSpace()->GfxColorSpace::isNonMarking()) {
        return;
    }

    m_words.push_back({});

    auto &currentWD = m_words.back();
    currentWD.cCoord.rx() = x;
    currentWD.cCoord.ry() = y;
    currentWD.dCoord.rx() = dx;
    currentWD.dCoord.ry() = dy;
    currentWD.oCoord.rx() = originX;
    currentWD.oCoord.ry() = originY;
    currentWD.fontName = state->getFont()->getName()->getCString();
    currentWD.bold = state->getFont()->isBold();
    currentWD.italic = state->getFont()->isItalic();
    currentWD.fontSize = state->getTransformedFontSize();

    currentWD.str = QString::fromUcs4(reinterpret_cast<uint*>(u), uLen);
    double *ctm = state->getCTM();

    point_transform_with_ctm(currentWD.cCoord, ctm);

    if (currentWD.str.isEmpty()) {
        m_words.pop_back();
    }
}



void page_output_dev::clip(GfxState *state)
{
    double xMin, yMin, xMax, yMax;
    state->getClipBBox(&xMin, &yMin, &xMax, &yMax);
    m_page_rect.setRect(xMin, yMin, xMax - xMin, yMax - yMin);
}

void page_output_dev::eoClip(GfxState *state)
{
    double xMin, yMin, xMax, yMax;
    state->getClipBBox(&xMin, &yMin, &xMax, &yMax);
    m_page_rect.setRect(xMin, yMin, xMax - xMin, yMax - yMin);
}

void page_output_dev::clipToStrokePath(GfxState *state)
{
    double xMin, yMin, xMax, yMax;
    state->getClipBBox(&xMin, &yMin, &xMax, &yMax);
    m_page_rect.setRect(xMin, yMin, xMax - xMin, yMax - yMin);
}




void page_output_dev::moveTo(const QPointF &point)
{
    m_pathes.back().push_back(point);
}

void page_output_dev::lineTo(const QPointF &point)
{
    m_pathes.back().push_back(point);
}

void page_output_dev::curveTo(const std::array<QPointF, 3> &points)
{
    QPointF p0 = m_pathes.back().back();
    qreal step = 1.0 / m_curve_resolution;
    qreal end = 1.0 - step / 2.0;
    for (qreal t = step; t < end; t += step) {
        qreal diff = (1.0 - t);
        qreal diff2 = diff * diff;
        qreal diff3 = diff2 * diff;
        QPointF r = diff3 * p0 + 3 * t * diff2 * points[0] + 3 * t * t * diff * points[1] + t * t * t * points[2];
        lineTo(r);
    }
}
