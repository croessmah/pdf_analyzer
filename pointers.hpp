#ifndef EASYTL_POINTERS_HPP
#define EASYTL_POINTERS_HPP
#include <exception>
//todo: macro
//todo: my exception base



namespace easytl
{
namespace errors
{

struct null_pointer : public std::exception
{
    const char *what() const noexcept override {
        return u8"null pointer checked";
    }
};

}//errors





template<typename T>
class not_null_pointer
{
public:
    not_null_pointer(T *data) :
        m_data(data)
    {
        if (m_data == nullptr) {
            throw errors::null_pointer();
        }
    }

    T* operator->() noexcept
    {
        return m_data;
    }

    T &operator*() noexcept
    {
        return *m_data;
    }

    operator T*() noexcept
    {
        return m_data;
    }
private:
    T *m_data;
};


}//easytl




#endif // EASYTL_POINTERS_HPP
