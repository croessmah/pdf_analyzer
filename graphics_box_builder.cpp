#include "graphics_box_builder.hpp"
#include "real_tools.hpp"
#include "optional.hpp"

#include <functional>
#include <iterator>
#include <utility>


namespace
{


bool is_hv_lines_parallel(const line_segment &f, const line_segment &s, qreal point_eps) noexcept
{
    return
        (f.is_vertical(point_eps) && s.is_vertical(point_eps)) ||
        (f.is_horizontal(point_eps) && s.is_horizontal(point_eps));
}


//Объединение вертикальных линий
void join_vertical_lines(std::vector<line_segment> &segments, qreal width_eps, qreal point_eps)
{
    //todo: проверку на вертикальность
    for (auto begin = segments.begin(); begin != segments.end(); ++begin) {
        for (auto current = std::next(begin); current != segments.end(); ) {
            //Если заданные значения у точек не равны, то они не лежат на одной прямой
            if (!qreal_compare(begin->p1().x(), current->p1().x(), point_eps)) {
                break;
            }
            //Обе либо вертикальные, либо горизонтальные
            if (!is_hv_lines_parallel(*begin, *current, point_eps)) {
                break;
            }

            if (
                qreal_compare(begin->width(), current->width(), point_eps) &&
                begin->try_add(*current, width_eps, point_eps)) {
                std::iter_swap(current, std::prev(segments.end()));
                segments.pop_back();
            } else {
                ++current;
            }
        }
    }
}


//Объединение горизонтальных линий
void join_horizontal_lines(std::vector<line_segment> &segments, qreal width_eps, qreal point_eps)
{
    //todo: проверку на горизонтальность
    for (auto begin = segments.begin(); begin != segments.end(); ++begin) {
        for (auto current = std::next(begin); current != segments.end(); ) {
            //Если заданные значения у точек не равны, то они не лежат на одной прямой
            if (!qreal_compare(begin->p1().y(), current->p1().y(), point_eps)) {
                break;
            }
            //Обе либо вертикальные, либо горизонтальные
            if (!is_hv_lines_parallel(*begin, *current, point_eps)) {
                break;
            }

            if (
                qreal_compare(begin->width(), current->width(), point_eps) &&
                begin->try_add(*current, width_eps, point_eps)) {
                std::iter_swap(current, std::prev(segments.end()));
                segments.pop_back();
            } else {
                ++current;
            }
        }
    }
}

template<typename SourceContainer, typename DestinationContainer, typename Func>
void move_elements_if(SourceContainer &source, DestinationContainer &destination, Func &&func)
{
    for (auto begin = source.begin(); begin != source.end();) {
        if (func(*begin)) {
            destination.emplace_back(std::move(*begin));
            begin = source.erase(begin);
        } else {
            ++begin;
        }
    }
}

/*
bool point_in_rect(const QRectF &rect, const QPointF &point)
{
    return
        qreal_between(rect.left(), point.x(), rect.right())
        && qreal_between(rect.top(), point.y(), rect.bottom());
}*/


enum CS_CODE : uint
{
    CS_LEFT = 0b0001,
    CS_RIGHT = 0b0010,
    CS_BOTTOM = 0b0100,
    CS_TOP = 0b1000
};


uint cohen_sutherland_code(const QRectF &rect, const QPointF &point)
{
    uint code = 0;
    if (rect.right() < point.x()) {
        code = CS_RIGHT;
    } else if (point.x() < rect.left()) {
        code = CS_LEFT;
    }
    if (point.y() < rect.top()) {
        code |= CS_TOP;
    } else if (rect.bottom() < point.y()) {
        code |= CS_BOTTOM;
    }
    return code;
};



bool clip_by_rect(const QRectF &rect, QPointF &p1, QPointF &p2) noexcept
{
    struct code_point_t {
        code_point_t(QPointF *p, uint *c) : point(p), code(c) {}
        QPointF *point;
        uint *code;
    };

    //Получаем коды
    uint p1_code = cohen_sutherland_code(rect, p1);
    uint p2_code = cohen_sutherland_code(rect, p2);

    code_point_t pr1{&p1, &p1_code};
    code_point_t pr2{&p2, &p2_code};

    while (p1_code | p2_code) {
        //Если линия не пересекает прямоугольник
        if (p1_code & p2_code) {
            return false;
        }
        //Делаем pr1 - всегда внешней точкой.
        if (*pr1.code == 0) {
            std::swap(pr1, pr2);
        }

        //
        if ((*pr1.code & CS_LEFT) || (*pr1.code & CS_RIGHT)) {
            qreal x = (*pr1.code & CS_LEFT)? rect.left(): rect.right();
            pr1.point->ry() = get_y_value_by_x(*pr1.point, *pr2.point, x);
            pr1.point->rx() = x;
        } else if ((*pr1.code & CS_BOTTOM) || (*pr1.code & CS_TOP)) {
            qreal y = (*pr1.code & CS_TOP)? rect.top(): rect.bottom();
            pr1.point->rx() = get_x_value_by_y(*pr1.point, *pr2.point, y);
            pr1.point->ry() = y;
        }

        *pr1.code = cohen_sutherland_code(rect, *pr1.point);
    }
    //Теперь обе точки в прямоугольнике
    return true;
}



bool clip_line_by_rect(const QRectF &rect, line_segment& segment)
{
    QPointF p1 = segment.p1();
    QPointF p2 = segment.p2();
    if (clip_by_rect(rect, p1, p2)) {
        segment.p1(p1);
        segment.p2(p2);
        return true;
    }
    return false;
}


//Распределение точек по сегментам
void pathes_to_segments(
    const std::list<points_path> &pathes,
    graphics_box_builder::container_t &horizontal_segments,
    graphics_box_builder::container_t &vertical_segments,
    graphics_box_builder::container_t &other_segments,
    qreal point_eps
    )
{
    for (auto &path: pathes) {
        for (size_t i = 1; i < path.size(); ++i) {
            //todo: добавить style
            line_segment segment(QLineF{path[i-1], path[i]}, path.width());
            if (!segment.is_point(point_eps)) {
                if (segment.is_horizontal(point_eps)) {
                    horizontal_segments.emplace_back(segment);
                } else if (segment.is_vertical(point_eps)){
                    vertical_segments.emplace_back(segment);
                } else {
                    other_segments.emplace_back(segment);
                }
            }
        }
    }
}


void sort_by_x(graphics_box_builder::container_t &c, qreal point_eps)
{
    auto sort_cmp = [point_eps] (const line_segment &f, const line_segment &s) {
        return qreal_less_compare(f.x1(), s.x1(), point_eps);
    };
    std::sort(c.begin(), c.end(), sort_cmp);
}


void sort_by_y(graphics_box_builder::container_t &c, qreal point_eps)
{
    auto sort_cmp = [point_eps] (const line_segment &f, const line_segment &s) {
        return qreal_less_compare(f.y1(), s.y1(), point_eps);
    };
    std::sort(c.begin(), c.end(), sort_cmp);
}

void sort_by_lex_left(graphics_box_builder::container_t &c, qreal point_eps)
{
    auto sort_cmp = [point_eps] (const line_segment &f, const line_segment &s) {
        return point_lex_less_compare(f.p1(), s.p1(), point_eps);
    };
    std::sort(c.begin(), c.end(), sort_cmp);
}


//Определение пересечений горизонтальных и вертикальных линий
using intersected_container_iterator_t = graphics_box_builder::container_t::const_iterator;
using intersected_container_with_iterators_t = std::vector<intersected_container_iterator_t>;
using intersected_map_t = std::vector<std::pair<intersected_container_with_iterators_t, intersected_container_iterator_t>>;
intersected_map_t create_intersected_map(
    const graphics_box_builder::container_t &horizontal_segments,
    const graphics_box_builder::container_t &vertical_segments,
    qreal point_eps
    )
{
    intersected_map_t intersect_map;
    for (auto v_begin = vertical_segments.begin(); v_begin != vertical_segments.end(); ++v_begin) {
        qreal v_y_min = std::min(v_begin->y1(), v_begin->y2()) - 2;
        qreal v_y_max = std::max(v_begin->y1(), v_begin->y2()) + 2;
        qreal v_x = v_begin->x1();
        intersected_container_with_iterators_t segment_intersected;
        //Проход по всем горизонтальным линиям в поисках пересечений
        for (auto h_begin = horizontal_segments.begin(); h_begin != horizontal_segments.end(); ++h_begin) {
            qreal h_y = h_begin->y1();
            //Если горизонтальная линия лежит выше вертикальной, то
            if (qreal_less_compare(h_y, v_y_min, point_eps)) {
                continue;//Пропускаем её
            }
            //Если горизонтальная линия лежит ниже вертикальной, то
            if (qreal_less_compare(v_y_max, h_y, point_eps)) {
                break;//можно закончить обработку, т.к. горизонтальные линии сортированы по координате y
            }
            //todo: подобрать более точный коэффициэнт смещения (например, 1)
            qreal h_x_min = std::min(h_begin->x1(), h_begin->x2()) - 2;
            qreal h_x_max = std::max(h_begin->x1(), h_begin->x2()) + 2;
            //Если линии не пересекаются, то
            if (qreal_less_compare(v_x, h_x_min, point_eps) || qreal_less_compare(h_x_max, v_x, point_eps)) {
                continue;//выходим
            }
            //Добавляем итератор на текущую линию в список пересекающиеся
            segment_intersected.emplace_back(h_begin);
        }
        //Если пересечений меньше двух, то можно не добавлять в обработку,
        //т.к. из таких линий нельзя составить прямоугольник.
        if (segment_intersected.size() > 2) {
            intersect_map.emplace_back(std::move(segment_intersected), v_begin);
        }
    }
    return intersect_map;
}

//Поиск прямоугольников
std::vector<QRectF> create_rectangles_from_intersected_map(const intersected_map_t &intersect_map)
{
    std::vector<QRectF> rectangles;
    //todo: добавить более точный поиск, не зависящий от перекрытия рамки
    //Проходим по всем соединениям
    for (auto begin = intersect_map.begin(), end = intersect_map.end(); begin != end; ++begin) {
        //Верхний левый угол - это пересечение вертикальной линии с верхней горизонтальной
        QPointF left_top(begin->second->x1(), begin->first.front()->y1());
        //За нижний правый угол принимаем пересечение вертикальной линии с нижней
        QPointF right_bottom(left_top.x(), begin->first.back()->y1());

        bool is_found_rectangle = false;
        //Проходим по всем пересечениям
        for (auto v_iter = intersect_map.begin(); v_iter != end; ++v_iter) {
            if (v_iter == begin) {
                continue;
            }
            //Ищем, содержится ли в пересечении линии верхнего и нижнего пересечений,
            //таким образом ищем вертикальные линии,
            //которые пересекаются с теми же горизонтальными, что и заданная линия
            auto find_top = std::find(v_iter->first.begin(), v_iter->first.end(), begin->first.front());
            auto find_down = std::find(find_top, v_iter->first.end(), begin->first.back());
            //Если не пересекаются, то
            if (find_top == v_iter->first.end() || find_down == v_iter->first.end()) {
                continue;//пропускаем
            }
            //Если вертикальная линия левее левой границы
            if (v_iter->second->x1() < left_top.rx()) {
                left_top.rx() = v_iter->second->line().x1();//То смещаем левую границу
            }
            //Если вертикальная линия правее правой границы
            if (right_bottom.rx() < v_iter->second->x1()) {
                right_bottom.rx() = v_iter->second->x1();//То смещаем правую границу
            }
            is_found_rectangle = true;
        }
        //Добавляем прямоугольник
        if (is_found_rectangle) {
            rectangles.emplace_back(left_top, right_bottom);
        }
    }
    return rectangles;
}


void erase_intersected_rectangles(std::vector<QRectF> &rectangles)
{
    //Проходим по всем прямоугольникам и удаляем вложенные, либо пересекающиеся
    for(auto begin = rectangles.begin(); begin != rectangles.end(); ++begin) {
        for (auto current = std::next(begin); current != rectangles.end();) {
            if (begin->contains(*current)) {
                current = rectangles.erase(current);
            } else {
                if (begin->intersects(*current) || current->contains(*begin)) {
                    if ((begin->width() * begin->height()) < (current->width() * current->height())) {
                        *begin = *current;
                    }
                    current = rectangles.erase(current);
                } else {
                    ++current;
                }
            }
        }
    }
}



graphics_box_builder::container_t distribution_segments_from_rect(
    const QRectF &rect,
    graphics_box_builder::container_t &lines,
    qreal point_eps
    )
{
    graphics_box_builder::container_t lines_in_box;
    lines_in_box.reserve(1000);
    for (line_segment line: lines) {
        if (clip_line_by_rect(rect, line) && !line.is_vertical(point_eps)) {
            lines_in_box.emplace_back(line);
        }
    }
    return lines_in_box;
}







graphics_box::container_t make_pathes(graphics_box_builder::container_t &segments, qreal point_eps, qreal width_eps)
{
    struct line_is_left
    {
       line_is_left(qreal eps) : m_eps(eps) {}
       bool operator() (const line_segment &segment, const QPointF &point) const noexcept {
           return point_lex_less_compare(segment.p1(), point, m_eps);
       }
       bool operator() (const QPointF &point, const line_segment &segment) const noexcept {
           return point_lex_less_compare(point, segment.p1(), m_eps);
       }
       qreal m_eps;
    };

    graphics_box::container_t pathes;

    if (segments.empty()) {
        return pathes;
    }

    for (auto &segment: segments) {
        segment.line_left_to_right();
    }

    sort_by_lex_left(segments, point_eps);

    auto is_equal_width_and_style = [width_eps] (qreal w1, qreal w2, int s1, int s2) {
        return s1 == s2 && qreal_compare(w1, w2, width_eps);
    };

    while (!segments.empty()) {
        points_path current_path;
        current_path.push_back(segments.front().p1());
        current_path.push_back(segments.front().p2());
        current_path.style(segments.front().style());
        current_path.width(segments.front().width());
        segments.erase(segments.begin());
        while (true) {
            auto eq_range = std::equal_range(segments.begin(), segments.end(), current_path.back(), line_is_left(point_eps));

            std::vector <graphics_box_builder::container_t::iterator> eq_iters;
            std::size_t eq_iters_size = 0;
            for (auto beg = eq_range.first; beg != eq_range.second; ++beg) {
                if (is_equal_width_and_style(current_path.width(), beg->width(), current_path.style(), beg->style())) {
                    eq_iters.push_back(beg);
                    ++eq_iters_size;
                }
            };

            if (eq_iters_size == 1) {
                current_path.push_back(eq_iters.front()->p2());
                segments.erase(eq_iters.front());
            } else {
                break;
            }
        }
        pathes.emplace_back(std::move(current_path));
    }

    return pathes;
}



} //internal linkage


graphics_box_builder::graphics_box_builder()
{

}


void graphics_box_builder::rebuild(const std::list<points_path> &pathes, qreal point_eps, qreal width_eps, qreal min_box_size, std::ostream &logger)
{
    container_t horizontal_segments;
    container_t vertical_segments;
    container_t other_segments;
    horizontal_segments.reserve(10000);
    vertical_segments.reserve(10000);
    other_segments.reserve(10000);

    logger << "start analize lines..." << std::endl;
    pathes_to_segments(pathes, horizontal_segments, vertical_segments, other_segments, point_eps);



    logger << "segments count: " << (horizontal_segments.size() + vertical_segments.size() + other_segments.size()) << std::endl;


    //todo: можно выполнить в другом потоке одну из веток. Не забыть std::ref
    sort_by_x(vertical_segments, point_eps);
    sort_by_y(horizontal_segments, point_eps);
    //todo: можно выполнить в другом потоке одну из веток. Не забыть std::ref
    join_vertical_lines(vertical_segments, width_eps, point_eps);
    join_horizontal_lines(horizontal_segments, width_eps, point_eps);


    //Сравнение квадрата длины отрезка с квадратом минимального размера бокса
    qreal min_square_length = min_box_size * min_box_size;
    auto line_square_length_less = [min_square_length] (const line_segment &segment) {
        return segment.square_length() < min_square_length;
    };

    //Отсеивание линий короче определенной длины (для ускорения обработки)
    move_elements_if(horizontal_segments, other_segments, line_square_length_less);
    move_elements_if(vertical_segments, other_segments, line_square_length_less);

    //todo: можно выполнить в другом потоке одну из веток. Не забыть std::ref
    sort_by_x(vertical_segments, point_eps);
    sort_by_y(horizontal_segments, point_eps);

    logger
        << "segments count: "
        << (horizontal_segments.size() + vertical_segments.size() + other_segments.size())
        << "\n"
        << "horizontal segments count: " << horizontal_segments.size() << "\n"
        << "vertical segments count: " << vertical_segments.size()  << "\n"
        << "other segments count: " << other_segments.size() << std::endl;


    intersected_map_t intersect_map = create_intersected_map(horizontal_segments, vertical_segments, point_eps);

    logger << "intersected lines: " << intersect_map.size() << std::endl;

    std::vector<QRectF> rectangles = create_rectangles_from_intersected_map(intersect_map);

    erase_intersected_rectangles(rectangles);

    std::cout << "rectangles count: " << rectangles.size() << std::endl;

    //Определяем параметры сетки
    container_t segments_grid;
    std::list<graphics_box> graphics_boxes;

    for(auto &rectangle: rectangles) {
        QRectF rect;
        rect.setLeft(rectangle.left() + 1);
        rect.setTop(rectangle.top() + 1);
        rect.setRight(rectangle.right() - 1);
        rect.setBottom(rectangle.bottom() - 1);
        auto is_vertical_grid_line = [rect] (const line_segment &line) {
            return
                qreal_between(rect.left(), line.x1(), rect.right())
                && qreal_two_between(line.y1(), rect.top(), rect.bottom(), line.y2());
        };
        auto is_horizontal_grid_line = [rect] (const line_segment &line) {
            return
               qreal_between(rect.top(), line.y1(), rect.bottom())
               && qreal_two_between(line.x1(), rect.left(), rect.right(), line.x2());
        };
        size_t vs_size = vertical_segments.size();
        size_t hs_size = horizontal_segments.size();
        //Отделение вертикалей сетки
        move_elements_if(vertical_segments, segments_grid, is_vertical_grid_line);
        //Отделение горизонталей сетки
        move_elements_if(horizontal_segments, segments_grid, is_horizontal_grid_line);

        unsigned int v_count = vs_size - vertical_segments.size();
        unsigned int h_count = hs_size - horizontal_segments.size();

        if (!(v_count && h_count)) {
            continue;
        }


        graphics_boxes.emplace_back(rectangle, v_count + 1, h_count + 1);


        //graphics_boxes.back().set_pathes(std::move(lines_in_box));
    }


    other_segments.insert(other_segments.end(), horizontal_segments.begin(), horizontal_segments.end());
    //todo: решить проблему вертикальных сегментов
    //other_segments.insert(other_segments.end(), vertical_segments.begin(), vertical_segments.end());


    for(graphics_box &current_box: graphics_boxes) {
        QRectF rect;
        rect.setLeft(current_box.rect().left() + 1);
        rect.setTop(current_box.rect().top() + 1);
        rect.setRight(current_box.rect().right() - 1);
        rect.setBottom(current_box.rect().bottom() - 1);
        graphics_box_builder::container_t lines_in_box =
            distribution_segments_from_rect(rect, other_segments, point_eps);
        //todo: составить пути
        graphics_box::container_t p = make_pathes(lines_in_box, point_eps, width_eps);
        //todo: объединить пути в графики
        //todo: определить минимальное количество точек в пути
        //p.erase(std::remove_if(p.begin(), p.end(), [](const points_path &path) { return path.size() < 3; }), p.end());
        logger << "Box pathes count: " << p.size() << std::endl;
    }

    logger << "Boxes:\n";
    for (auto &box: graphics_boxes) {
        logger
            << "Box grid: "
            << box.width_in_cells()
            << ","
            << box.height_in_cells()
            << "; Box rect: "
            << box.rect().left() << ", "
            << box.rect().top() << ", "
            << box.rect().right() << ", "
            << box.rect().bottom()
            << std::endl;
    }

    //Сохраняем новые параметры
    m_boxes.swap(graphics_boxes);
}
