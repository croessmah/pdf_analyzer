#include "graphics_box.hpp"
#include "points_path.hpp"


graphics_box::graphics_box(const QRectF &rect, unsigned int x_cells, unsigned int y_cells, const graphics_box::container_t &pathes) :
    m_width_cells(x_cells),
    m_height_cells(y_cells),
    m_rect(rect)
{
    for (auto &path: pathes) {
        m_pathes.emplace_back(std::make_shared<points_path>(path));
    }
}

graphics_box::graphics_box(const QRectF &rect, unsigned int x_cells, unsigned int y_cells, graphics_box::container_t  &&pathes) noexcept:
    m_width_cells(x_cells),
    m_height_cells(y_cells),
    m_rect(rect)
{
    for (auto &&path: pathes) {
        m_pathes.emplace_back(std::make_shared<points_path>(std::move(path)));
    }
}

unsigned int graphics_box::width_in_cells() const noexcept
{
    return m_width_cells;
}

unsigned int graphics_box::height_in_cells() const noexcept
{
    return m_height_cells;
}

void graphics_box::set_pathes(const graphics_box::container_t &pts) noexcept
{
    m_pathes.clear();
    for (auto &&path: pts) {
        m_pathes.emplace_back(std::make_shared<points_path>(path));
    }
}

const graphics_box::shared_pathes_container_t  &graphics_box::pathes() const noexcept
{
    return m_pathes;
}

graphics_box::shared_pathes_container_t::value_type graphics_box::path(unsigned int index) const noexcept
{
    return m_pathes[index];
}

const QRectF &graphics_box::rect() const noexcept
{
    return m_rect;
}

void graphics_box::swap(graphics_box &src) noexcept
{
    std::swap(m_width_cells, src.m_width_cells);
    std::swap(m_height_cells, src.m_height_cells);
    std::swap(m_rect, src.m_rect);
    std::swap(m_pathes, src.m_pathes);
}
