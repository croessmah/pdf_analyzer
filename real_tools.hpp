#ifndef REALTOOLS_H
#define REALTOOLS_H

#include <QPointF>
#include <QLineF>

inline bool qreal_compare(qreal f, qreal s, qreal point_eps) noexcept
{
    return (qAbs(f - s) <= point_eps * qMin(qAbs(f), qAbs(s)));
}


inline bool qreal_less_compare(qreal f, qreal s, qreal point_eps) noexcept
{
    return f < s && !qreal_compare(f, s, point_eps);
}


inline bool qreal_less_equal_compare(qreal f, qreal s, qreal point_eps) noexcept
{
    return f < s || qreal_compare(f, s, point_eps);
}


inline bool point_compare(const QPointF &f, const QPointF &s, qreal point_eps) noexcept
{
    return qreal_compare(f.x(), s.x(), point_eps) && qreal_compare(f.y(), s.y(), point_eps);
}


inline bool point_lex_less_compare(const QPointF &f, const QPointF &s, qreal point_eps) noexcept
{
    if (qreal_compare(f.x(), s.x(), point_eps)) {
        return !qreal_compare(f.y(), s.y(), point_eps) && (f.y() < s.y());
    }
    return f.x() < s.x();
}



inline bool qreal_between(qreal min, qreal mid, qreal max) noexcept
{
    return min <= mid && mid <= max;
}

inline bool qreal_two_between(qreal b1, qreal mid1, qreal mid2, qreal b2) noexcept
{
    if (b2 < b1) {
        std::swap(b2, b1);
    }
    return qreal_between(b1, mid1, b2) && qreal_between(b1, mid2, b2);
}


inline double get_y_value_by_x(qreal x1, qreal y1, qreal x2, qreal y2, qreal x) noexcept
{
    return y1 + (y1 - y2) * (x - x1) / (x1 - x2);
}


inline double get_x_value_by_y(qreal x1, qreal y1, qreal x2, qreal y2, qreal y) noexcept
{
    return x1 + (x1 - x2) * (y - y1) / (y1 - y2);
}


inline double get_y_value_by_x(const QPointF &p1, const QPointF &p2, qreal x) noexcept
{
    return get_y_value_by_x(p1.x(), p1.y(), p2.x(), p2.y(), x);
}


inline double get_x_value_by_y(const QPointF &p1, const QPointF &p2, qreal y) noexcept
{
    return get_x_value_by_y(p1.x(), p1.y(), p2.x(), p2.y(), y);
}


#endif // REALTOOLS_H
