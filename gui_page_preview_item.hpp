#ifndef GUI_PAGE_PREVIEW_ITEM_HPP
#define GUI_PAGE_PREVIEW_ITEM_HPP

#include <QListWidgetItem>
#include "pointers.hpp"
QT_FORWARD_DECLARE_CLASS(QPixmap)
QT_FORWARD_DECLARE_CLASS(QListWidget)


class gui_page_preview_item :
    public QListWidgetItem
{
public:
    explicit gui_page_preview_item(int page, const QPixmap &preview, easytl::not_null_pointer<QListWidget> parent);
    int page() const noexcept;
private:
    int m_page;
};

#endif // GUI_PAGE_PREVIEW_ITEM_HPP
