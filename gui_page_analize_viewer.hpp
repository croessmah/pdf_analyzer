#ifndef GUI_PAGE_ANALIZE_VIEWER_HPP
#define GUI_PAGE_ANALIZE_VIEWER_HPP

#include <QWidget>
#include <memory>
#include "pdf_document.hpp"

QT_FORWARD_DECLARE_CLASS(QGraphicsScene)
QT_FORWARD_DECLARE_CLASS(QGraphicsView)

class gui_page_analize_viewer : public QWidget
{
    Q_OBJECT
public:
    explicit gui_page_analize_viewer(QWidget *parent = 0);

signals:

public slots:
    void load_geometry(std::shared_ptr<pdf_document> doc, int page);
private:
    QGraphicsScene *m_scene;
    QGraphicsView *m_view;
    //int m_mode;//временно не используется
};

#endif // GUI_PAGE_ANALIZE_VIEWER_HPP
