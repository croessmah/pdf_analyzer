#ifndef GRAPHIC_GRID_HPP
#define GRAPHIC_GRID_HPP
#include <QPointF>

class graphic_grid
{
public:
    graphic_grid();
    const QPoint &cells_count() const noexcept;
    const QPointF &cells_weight() const noexcept;
    const QPointF &zero() const noexcept;
    void cells_count(int x, int y) noexcept;
    void cells_weight(qreal w, qreal h) noexcept;
    void zero(qreal x, qreal y) noexcept;
    QPointF mapped_point(const QPointF &point) const noexcept;
private:
    QPoint m_cells_count;
    QPointF m_cells_weight;
    QPointF m_zero;
};

#endif // GRAPHIC_GRID_HPP
