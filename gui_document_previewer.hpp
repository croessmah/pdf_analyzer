#ifndef GUI_DOCUMENT_PREVIEWER_HPP
#define GUI_DOCUMENT_PREVIEWER_HPP

#include <QWidget>
#include <memory>
#include "pdf_document.hpp"
#include "document_loader.hpp"

QT_FORWARD_DECLARE_CLASS(QThread)
QT_FORWARD_DECLARE_CLASS(gui_page_preview_list)
QT_FORWARD_DECLARE_CLASS(gui_collapse_widget)

class gui_document_previewer : public QWidget
{
    Q_OBJECT
public:
    explicit gui_document_previewer(QWidget *parent = 0);
signals:
    void document_load_failed();
    void page_choosed(std::shared_ptr<pdf_document> document, int page);

public slots:

private slots:
    void choose_page(int page);
    void choose_document();
private:
    void clear_preview();
    std::shared_ptr<pdf_document> m_doc;
    std::unique_ptr<document_loader> m_loader;
    QThread *m_load_thread;
    gui_collapse_widget *m_collapse_widget;
    gui_page_preview_list *m_preview_list;
};

#endif // GUI_DOCUMENT_PREVIEWER_HPP
