#ifndef LINESEGMENT_HPP
#define LINESEGMENT_HPP

#include <QtGlobal>
#include <QLineF>
class PLine;

struct line_segment
{
public:
    line_segment(const QLineF &line, double width, int style = 0);
    //todo: учет стиля
    bool try_add(const line_segment &line, qreal width_eps, qreal point_eps) noexcept;

    bool is_vertical(qreal point_eps) const noexcept;

    bool is_horizontal(qreal point_eps) const noexcept;

    bool is_point(qreal point_eps) const noexcept;

    void line_left_to_right() noexcept;

    qreal length() const noexcept;
    qreal square_length() const noexcept;
    int style() const noexcept;
    void style(int s) noexcept;
    const QLineF &line() const noexcept;
    QPointF p1() const noexcept;
    QPointF p2() const noexcept;
    void p1(const QPointF &p) noexcept;
    void p2(const QPointF &p) noexcept;
    qreal x1() const noexcept;
    qreal y1() const noexcept;
    qreal x2() const noexcept;
    qreal y2() const noexcept;

    double width() const noexcept;
    void width(double line_width) noexcept;

private:
    static QLineF pline_to_qlinef(const PLine &line) noexcept;
    QLineF m_line;
    double m_width;
    int m_style;
};



#endif // LINESEGMENT_HPP
