#include "document_loader.hpp"
#include "pdf_document.hpp"
#include "page_painter.hpp"
#include <QString>
document_loader::document_loader(QObject *parent) : QObject(parent)
{

}

std::shared_ptr<pdf_document> document_loader::document() const noexcept
{
    return m_doc;
}

void document_loader::stop_load_preview()
{
    m_load_enabled = false;
}

void document_loader::load(QString file_name)
{
    emit document_started(file_name);
    std::shared_ptr<pdf_document> doc(std::make_shared<pdf_document>());
    #ifdef _WIN32
        doc->load(file_name.toStdWString().c_str());
    #else
        doc->load(fileName.toStdString().c_str());
    #endif
    m_doc = std::move(doc);
    emit document_finished(m_doc);
}

void document_loader::create_pages_geometry()
{
    create_pages(m_doc);
}



void document_loader::create_pages(std::shared_ptr<pdf_document> doc)
{
    page_painter painter;
    m_load_enabled = true;
    for (int i = 1; m_load_enabled && i <= doc->pages_count(); ++i) {
        auto page_geometry = doc->get_page_geometry(i, 96);
        painter.paint(*page_geometry);
        if (m_load_enabled) {
            emit page_loaded(i, painter.pixmap());
        }
    }
    m_load_enabled = true;
    emit page_load_finished();
}
