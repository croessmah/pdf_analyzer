#ifndef ADVDATA_HPP
#define ADVDATA_HPP

#include <QString>
#include "ChoosedGraphData.hpp"


struct ResultData
{
    ResultData(int type, QString yxType, ChoosedGraphData::GraphData&& data)
        : mType(type)
        , mYXType(yxType)
        , mData(std::move(data))
    {
    }

    ResultData(const ResultData&) = default;
    ResultData(ResultData&&) = default;
    ResultData& operator=(const ResultData&) = default;
    ResultData& operator=(ResultData&&) = default;
    ~ResultData() = default;
    int mType;
    QString mYXType;
    ChoosedGraphData::GraphData mData;
};


#endif // ADVDATA_HPP
