#ifndef GUI_ARROW_BUTTON_HPP
#define GUI_ARROW_BUTTON_HPP

#include <QWidget>
#include <QSize>

QT_FORWARD_DECLARE_CLASS(QBrush);
QT_FORWARD_DECLARE_CLASS(QPalette);


class gui_arrow_button : public QWidget
{
    Q_OBJECT
public:
    enum class directions : unsigned
    {
        ToRight,
        ToLeft,
        ToTop,
        ToBottom,
    };
    explicit gui_arrow_button(directions dir, QWidget *parent = 0);
    ~gui_arrow_button();
    void set_direction(directions dir);
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    bool get_state() const noexcept;
    QSize get_size() const noexcept;
signals:
    void toggled(bool);
public slots:
    void toggle();
private:
    void update_palette();
    void destroy_resources() noexcept;
    QBrush *m_brush_forward;
    QBrush *m_brush_reverse;
    QSize m_size;
    bool m_state;
    bool m_last_mouse_left_press;
};

#endif // GUI_ARROW_BUTTON_HPP
