#ifndef OPTIONAL_HPP
#define OPTIONAL_HPP

#include <type_traits>
#include <utility>

template<typename T>
class optional
{
public:
    optional()
        : mDataPtr(nullptr)
    {
    }

    optional(optional &&v)
        : mDataPtr(nullptr)
    {
        if (v.mDataPtr) {
            new (buffer) T(std::move(*v.mDataPtr));
            mDataPtr = getPtr();
        }
    }

    optional(const T &v)
        : mDataPtr(nullptr)
    {
        new (buffer) T(v);
        mDataPtr = getPtr();
    }

    optional &operator=(optional &&v)
    {
        if (this == &v) {
            return *this;
        }
        clear();
        if (v.isSet()) {
            new (buffer) T(std::move(*v.mDataPtr));
            mDataPtr = getPtr();
        }
        return *this;
    }

    optional &operator=(const optional &v)
    {
        if (this == &v) {
            return *this;
        }
        clear();
        if (v.isSet()) {
            new (buffer) T(*v.mDataPtr);
            mDataPtr = getPtr();
        }
        return *this;
    }

    ~optional()
    {
        clear();
    }

    T *get()
    {
        return mDataPtr;
    }

    bool isSet() const
    {
        return mDataPtr != nullptr;
    }

    void clear()
    {
        if (mDataPtr) {
            mDataPtr->~T();
            mDataPtr = nullptr;
        }
    }

    void reset(const T &v)
    {
        clear();
        new (buffer) T(v);
        mDataPtr = getPtr();
    }


    optional(const optional &) = delete;


private:

    T *getPtr()
    {
        return reinterpret_cast<T*>(buffer);
    }

    T *mDataPtr;
    alignas(alignof(T)) unsigned char buffer[sizeof(T)];
};

#endif // OPTIONAL_HPP
