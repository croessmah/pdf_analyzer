#ifndef RESULTSAVEWINDOW_HPP
#define RESULTSAVEWINDOW_HPP

#include <QDialog>
#include <QJsonObject>
#include <QJsonArray>

class QLineEdit;
class QPushButton;
class QLabel;

#include "ResultData.hpp"

class ResultSaveWindow : public QDialog
{
    Q_OBJECT
public:
    explicit ResultSaveWindow(QWidget *parent = 0);
signals:
    void saveFinished();
    void generateFinished();
public slots:
    void generate(const std::list<ResultData>& data);
    void save();
private:
    QLineEdit *mPumpName;
    QLabel *mSaveResultLabel;
    QPushButton *mSave;
    QJsonArray mPumpsDataArray;
    QJsonArray mOtherDataArray;
};

#endif // RESULTSAVEWINDOW_HPP
