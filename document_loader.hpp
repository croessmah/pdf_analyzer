#ifndef DOCUMENT_LOADER_HPP
#define DOCUMENT_LOADER_HPP

#include <QObject>
#include <memory>
#include <atomic>

#include "pdf_document.hpp"

#include <QPixmap>

class document_loader : public QObject
{
    Q_OBJECT
public:
    explicit document_loader(QObject *parent = 0);
    std::shared_ptr<pdf_document> document() const noexcept;
    void stop_load_preview();
signals:
    void document_started(QString name);
    void page_loaded(int page, QPixmap);
    void page_load_finished();
    void progress_changed(QString status_message, double percent);
    void document_finished(std::shared_ptr<pdf_document> doc);
public slots:
    void load(QString fileName);
    void create_pages_geometry();
private:
    void create_pages(std::shared_ptr<pdf_document> doc);
    std::shared_ptr<pdf_document> m_doc;
    std::atomic_bool m_load_enabled;
};

#endif // DOCUMENT_LOADER_HPP
