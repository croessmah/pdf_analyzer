#include "WorkWidget.hpp"

#include <QResizeEvent>
#include <QPaintEvent>
#include <QPushButton>
#include <QDebug>//todo: delete
#include <QFrame>
#include <QMessageBox>
#include <QStringList>
#include <QListView>
#include <QPainter>
#include <QLabel>
#include <QStringListModel>
#include <QComboBox>
#include <QFrame>

#include <QVBoxLayout>
#include <QHBoxLayout>

#include "GraphicsControllWidget.hpp"
#include "PGraphicsGraph.hpp"
#include "CoordinatePage.hpp"
#include "PGraphicAnalizeViewer.hpp"
#include "ChoosedGraphData.hpp"
#include "ListGraphicsData.hpp"
#include "GlobalSettingsViewer.hpp"
#include "PdfPageChooser.hpp"
#include "ListItemGInfo.hpp"


WorkWidget::WorkWidget(QWidget *parent)
    : QWidget(parent)
    , mAnalizeViewer(new PGraphicAnalizeViewer())
    , mListGraphicsWidget(new ListGraphicsData())
    , mValuesList(new QListView())
    , mValuesListModel(new QStringListModel())
    , mGraphicsSetup(new GraphicsControllWidget())
    , mGSViewer (new GlobalSettingsViewer())
    , mPdfChooser (new PdfPageChooser())
{
    const int rightPanelWidth = 290;
    QWidget *horizontalLineWidget = new QWidget;
    mSaveWidget = new ResultSaveWindow(this);
    horizontalLineWidget->setFixedHeight(2);
    horizontalLineWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    horizontalLineWidget->setStyleSheet(QString("background-color: #404040;"));




    QHBoxLayout *mainLayout = new QHBoxLayout();
    QVBoxLayout *rightPanelLayout = new QVBoxLayout();


#ifdef DEMO
    QLabel *demoLabel = new QLabel("<H2 style=\"color: red\""">Демо версия</H2>");
    rightPanelLayout->addWidget(demoLabel);
    demoLabel->setMaximumSize(rightPanelWidth, 50);
#endif

    QPushButton *addToResultButton = new QPushButton(tr("Добавить данные к результату"));
    rightPanelLayout->addWidget(mListGraphicsWidget);
    rightPanelLayout->addWidget(mGraphicsSetup);
    rightPanelLayout->addWidget(mValuesList);
    rightPanelLayout->addWidget(addToResultButton);
    rightPanelLayout->addWidget(horizontalLineWidget);
    rightPanelLayout->addWidget(mGSViewer);
    rightPanelLayout->addWidget(mPdfChooser);

    rightPanelLayout->setMargin(5);

    mainLayout->addWidget(mAnalizeViewer);
    mainLayout->addLayout(rightPanelLayout);

    mainLayout->setSpacing(3);
    mainLayout->setMargin(0);

    this->setLayout(mainLayout);

    mListGraphicsWidget->setMaximumSize(rightPanelWidth, 800);
    mListGraphicsWidget->setMinimumSize(rightPanelWidth, 150);
    mGSViewer->setMaximumSize(rightPanelWidth, 150);
    mPdfChooser->setMaximumSize(rightPanelWidth, 100);
    mValuesList->setMaximumSize(rightPanelWidth, 80);
    mValuesList->setModel(mValuesListModel);
    mGraphicsSetup->setMaximumSize(rightPanelWidth, 100);
    horizontalLineWidget->setMaximumSize(rightPanelWidth, 2);
    connect(mAnalizeViewer,
            SIGNAL(rectChoosed(QRectF, std::list<PGraphicsGraph*>, std::list<PGraphicsText*>)),
            &mChoosedData,
            SLOT(load(QRectF, std::list<PGraphicsGraph*>, std::list<PGraphicsText*>))
    );
    connect(
        &mChoosedData,
        SIGNAL(loaded()),
        this,
        SLOT(choosedDataLoaded())
    );
    connect(
        mPdfChooser,
        SIGNAL(pageLoaded(CoordinatePage*)),
        this,
        SLOT(setCoordinatePage(CoordinatePage*))
    );
    connect(
        mPdfChooser,
        SIGNAL(pageLoadFailed()),
        this,
        SLOT(loadPageFailed())
    );
    connect(
        mPdfChooser,
        SIGNAL(documentLoadFailed()),
        this,
        SLOT(loadDocFailed())
    );
    connect(
        mGSViewer,
        SIGNAL(reload()),
        mPdfChooser,
        SLOT(reloadPage())
    );
    connect(
        mListGraphicsWidget,
        SIGNAL(itemSelected(bool, size_t)),
        this,
        SLOT(chooseGraphic(bool, size_t))
    );
    connect(
        mGraphicsSetup,
        SIGNAL(changed(QPointF,QPointF)),
        SLOT(scaleChanged(QPointF,QPointF))
    );
    connect(
        addToResultButton,
        SIGNAL(clicked()),
        SLOT(addCurrentDataToResult())
    );
    connect(
        this,
        SIGNAL(dataAdded()),
        SLOT(addedData())
    );
    connect(
        this,
        SIGNAL(dataAdded()),
        SLOT(clearState())
    );
    connect(
        this,
        SIGNAL(dataAdded()),
        mGraphicsSetup,
        SLOT(setInvalidDataType())
    );
    connect(
        this,
        &WorkWidget::analizeFinished,
        mSaveWidget,
        &ResultSaveWindow::generate
    );
    connect(
        mSaveWidget,
        &ResultSaveWindow::saveFinished,
        mSaveWidget,
        &ResultSaveWindow::close
    );
    connect(
        mSaveWidget,
        &ResultSaveWindow::saveFinished,
        this,
        &WorkWidget::clearResult
    );
    mGraphicsSetup->updateValues();
}


WorkWidget::~WorkWidget()
{
}



bool WorkWidget::setCoordinatePage(CoordinatePage *page)
{
    clearState();
    mResultData.clear();
    bool result = mAnalizeViewer->setCoordinatePage(*page);
    mGSViewer->settingsChanged();
    return result;
}


void WorkWidget::choosedDataLoaded()
{
    mListGraphicsWidget->setupParams(mChoosedData);
    mValuesListModel->setStringList(QStringList() << "is loaded");
}

void WorkWidget::loadPageFailed()
{
    QMessageBox(
        QMessageBox::Icon::Warning,
        tr("Ошибка"),
        tr("Загрузка страницы не удалась"),
        QMessageBox::StandardButton::Ok,
        this
    ).exec();
}

void WorkWidget::loadDocFailed()
{
    QMessageBox(
        QMessageBox::Icon::Warning,
        tr("Ошибка"),
        tr("Загрузка документа не удалась"),
        QMessageBox::StandardButton::Ok,
        this
    ).exec();
}

void WorkWidget::chooseGraphic(bool value, size_t index)
{
    Q_UNUSED(value);
    ChoosedGraphData::GraphData scaledData = mChoosedData.getData(index).makeCopyScaled(mNullXY, mMaxXY);
    QStringList valuesList;
    for (auto &line :scaledData.getLines()) {
        for (auto &point: line.getPoints()) {
            QString stringLine = QStringLiteral("Point [%1, %2]").arg(point.x()).arg(point.y());
            valuesList << stringLine;
        }
    }
    mValuesListModel->setStringList(valuesList);
}

void WorkWidget::scaleChanged(const QPointF &nullPoint, const QPointF &maxPoint)
{
    mNullXY = nullPoint;
    mMaxXY = maxPoint;
    ListItemGInfo* iCur = static_cast<ListItemGInfo*>(mListGraphicsWidget->currentItem());
    if (iCur != nullptr) {
        chooseGraphic(true, iCur->getIndex());
    }
}

void WorkWidget::addCurrentDataToResult()
{
    std::vector<ChoosedGraphData::GraphData> datas = mChoosedData.makeCopyAllScaled(mNullXY, mMaxXY);

    if (mAnalizeViewer->isAllHidden() && !mResultData.empty()) {
        emit analizeFinished(mResultData);
    }

    bool isData = false;
    for (auto it = datas.begin(); it != datas.end(); ++it) {
        if (it->getGraphic()->isActive()) {
            isData = true;
            break;
        }
    }
    if (!isData) {
        QMessageBox(
            QMessageBox::Icon::Warning,
            tr("Предупреждение"),
            tr("Нет данных для добавления"),
            QMessageBox::StandardButton::Ok,
            this
        ).exec();
        return;
    }
    int dataType = mGraphicsSetup->getDataType();
    QString yxType = mGraphicsSetup->getYXType();
    if (dataType < 1 || dataType > 2 || yxType.isEmpty()) {
        QMessageBox(
            QMessageBox::Icon::Warning,
            tr("Ошибка"),
            tr("Указан не корректный тип графиков"),
            QMessageBox::StandardButton::Ok,
            this
        ).exec();
        return;
    }


    for (auto it = datas.begin(); it != datas.end(); ++it) {
        if (!it->getGraphic()->isActive()) {
            continue;
        } else {
            //...
            if (it->getName().trimmed().isEmpty()) {
                QMessageBox(
                    QMessageBox::Icon::Warning,
                    tr("Ошибка"),
                    tr("Не указаны имена графиков"),
                    QMessageBox::StandardButton::Ok,
                    this
                ).exec();
                return;
            }
        }
        mResultData.emplace_back(dataType, yxType, std::move(*it));
        it->getGraphic()->hide();
    }
    emit dataAdded();
}

void WorkWidget::addedData()
{
    mValuesListModel->setStringList(QStringList() << tr("Данные добавлены к результату"));
    clearState();
    if (mAnalizeViewer->isAllHidden()) {
        emit analizeFinished(mResultData);
    }
}

void WorkWidget::clearState()
{
    mListGraphicsWidget->clearData();
    mChoosedData.clear();
    mAnalizeViewer->stateStartMode();
}

void WorkWidget::clearResult()
{
    clearState();
    mResultData.clear();
}


