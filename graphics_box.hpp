#ifndef GRAPHICS_BOX_HPP
#define GRAPHICS_BOX_HPP
#include <list>
#include <memory>
#include <QRectF>

#include "points_path.hpp"

class graphics_box
{
public:
    using container_t = std::vector<points_path>;
    using shared_pathes_container_t = std::vector<std::shared_ptr<points_path>>;
    graphics_box(const QRectF &rect, unsigned int x_cells, unsigned int y_cells, const container_t &pathes = {});
    graphics_box(const QRectF &rect, unsigned int x_cells, unsigned int y_cells, container_t &&pathes) noexcept;
    unsigned int width_in_cells() const noexcept;
    unsigned int height_in_cells() const noexcept;
    void set_pathes(const container_t &pts) noexcept;
    const shared_pathes_container_t &pathes() const noexcept;
    shared_pathes_container_t::value_type path(unsigned int index) const noexcept;
    const QRectF &rect() const noexcept;
    void swap(graphics_box &src) noexcept;
private:
    unsigned int m_width_cells;
    unsigned int m_height_cells;
    QRectF m_rect;
    shared_pathes_container_t m_pathes;
};



#endif // GRAPHICS_BOX_HPP
