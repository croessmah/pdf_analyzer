#ifndef GRAPHICS_BOX_BUILDER_HPP
#define GRAPHICS_BOX_BUILDER_HPP

#include <graphics_box.hpp>
#include <line_segment.hpp>
#include <points_path.hpp>

#include <iostream>
#include <vector>
#include <list>

class graphics_box_builder
{
public:
    using container_t = std::vector<line_segment>;
    graphics_box_builder();
    void rebuild(const std::list<points_path>& pathes, qreal point_eps, qreal width_eps, qreal min_box_size, std::ostream &logger = std::clog);
private:
    std::list<graphics_box> m_boxes;
    container_t m_grid_segments;
    container_t m_other_segments;
};

#endif // GRAPHICS_BOX_BUILDER_HPP
