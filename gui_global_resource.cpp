#include "gui_global_resource.hpp"

#include <QPixmap>
#include <QTransform>
#include <memory>
#include <QString>



const QString &get_gres_path()
{
    //todo:: исправить путь на нужный
    static QString res_path = "../pdf_analyzer/gres/";
    return res_path;
}


QPixmap *get_right_arrow() noexcept
{
    static std::unique_ptr<QPixmap> pixmap(new QPixmap(get_gres_path() + "1.bmp"));
    return pixmap.get();
}

QPixmap *get_left_arrow() noexcept
{
    static std::unique_ptr<QPixmap> pixmap(new QPixmap(get_right_arrow()->transformed(QTransform().scale(-1,1))));
    return pixmap.get();
}
