#ifndef PDF_ANALYZER_MAINWINDOW_HPP
#define PDF_ANALYZER_MAINWINDOW_HPP

#include <QPointF>
//#include <QMainWindow>
#include <QWidget>
#include <memory>

#include "ChoosedGraphData.hpp"
#include "ResultData.hpp"
#include "ResultSaveWindow.hpp"

class GraphicsControllWidget;
class PGraphicAnalizeViewer;
class ListGraphicsData;
class QListView;
class QStringListModel;
class GlobalSettingsViewer;
class PdfPageChooser;
class CoordinatePage;
class QResizeEvent;

//todo: добавить виджеты для названия насоса, кнопку готовности данных и названия шкал
class WorkWidget : public QWidget
{
    Q_OBJECT
public:
    WorkWidget(QWidget *parent = 0);
    ~WorkWidget();
signals:
    void dataAdded();
    void analizeFinished(const std::list<ResultData>&);
public slots:
    bool setCoordinatePage(CoordinatePage *page);
private slots:
    void choosedDataLoaded();
    void loadPageFailed();
    void loadDocFailed();
    void chooseGraphic(bool value, size_t index);
    void scaleChanged(const QPointF &nullPoint, const QPointF &maxPoint);
    void addCurrentDataToResult();
    void addedData();
private slots:
    void clearState();
    void clearResult();
private:
    ResultSaveWindow *mSaveWidget;
    PGraphicAnalizeViewer *mAnalizeViewer;
    ListGraphicsData *mListGraphicsWidget;
    ChoosedGraphData mChoosedData;
    QListView *mValuesList;
    QStringListModel *mValuesListModel;
    GraphicsControllWidget *mGraphicsSetup;
    GlobalSettingsViewer *mGSViewer;
    PdfPageChooser *mPdfChooser;
    QPointF mNullXY;
    QPointF mMaxXY;
    std::list<ResultData> mResultData;
};
//todo: add names

#endif // PDF_ANALYZER_MAINWINDOW_HPP
