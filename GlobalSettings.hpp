#ifndef GLOBALSETTINGS_HPP
#define GLOBALSETTINGS_HPP

#include <vector>

class GlobalSettings
{
public:
    void reset();
    double getRecommendLineWidth() const noexcept;
    void setRecommendLineWidth(double value) noexcept;
    double getRecommendLineWidthEps() const noexcept;
    void setRecommendLineWidthEps(double value) noexcept;
    double getNonRecommendLineWidth() const noexcept;
    void setNonRecommendLineWidth(double value) noexcept;
    double getNonRecommendLineWidthEps() const noexcept;
    void setNonRecommendLineWidthEps(double value) noexcept;
    double getPointEps() const noexcept;
    void setPointEps(double value) noexcept;
    double getPageClipLeft() const noexcept;
    void setPageClipLeft(double value) noexcept;
    double getPageClipTop() const noexcept;
    void setPageClipTop(double value) noexcept;
    double getPageClipRight() const noexcept;
    void setPageClipRight(double value) noexcept;
    double getPageClipBottom() const noexcept;
    void setPageClipBottom(double value) noexcept;
    const std::vector<double> &getFoundedWidth() const noexcept;
    void setFoundedWidth(const std::vector<double> &values);
    GlobalSettings(const GlobalSettings&) = delete;
    GlobalSettings &operator=(const GlobalSettings&) = delete;
    GlobalSettings(GlobalSettings&&) = delete;
    GlobalSettings &operator=(const GlobalSettings&&) = delete;
    ~GlobalSettings() = default;
private:
    GlobalSettings();
    friend GlobalSettings &gSettings();
    double mRecommendLineWidth;
    double mRecommendLineWidthEsp;
    double mNonRecommendLineWidth;
    double mNonRecommendLineWidthEsp;
    double mPointEps;
    double mPageClipLeft;
    double mPageClipTop;
    double mPageClipRigth;
    double mPageClipBottom;
    std::vector<double> mFoundedWidth;
};


GlobalSettings &gSettings();


#endif // GLOBALSETTINGS_HPP
