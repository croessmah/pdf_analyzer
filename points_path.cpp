#include "points_path.hpp"
#include "real_tools.hpp"


points_path::points_path() :
    m_style(0),
    m_width(1.0)
{
}

void points_path::push_back(const QPointF &point)
{
    m_points.push_back(point);
}

const QPointF &points_path::operator[](size_t index) const noexcept
{
    return m_points[index];
}

QPointF &points_path::operator[](size_t index) noexcept
{
    return m_points[index];
}

const QPointF &points_path::front() const noexcept
{
    return m_points.front();
}

const QPointF &points_path::back() const noexcept
{
    return m_points.back();
}


size_t points_path::size() const noexcept
{
    return m_points.size();
}

bool points_path::empty() const noexcept
{
    return m_points.empty();
}

void points_path::width(qreal w) noexcept
{
    m_width = w;
}

qreal points_path::width() const noexcept
{
    return m_width;
}

int points_path::style() const noexcept
{
    return m_style;
}

void points_path::style(int s) noexcept
{
    m_style = s;
}

void points_path::clear() noexcept
{
    m_points.clear();
}

points_path::iterator_t points_path::begin() noexcept
{
    return m_points.begin();
}


points_path::iterator_t points_path::end() noexcept
{
    return m_points.end();
}


points_path::const_iterator_t points_path::begin() const noexcept
{
    return m_points.cbegin();
}


points_path::const_iterator_t points_path::end() const noexcept
{
    return m_points.cend();
}

void points_path::reserve(size_t cap)
{
    m_points.reserve(cap);
}
