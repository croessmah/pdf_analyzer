#include "pdf_document.hpp"
#include <cwchar>

#include <QDebug>
#include <thread>

#ifdef _WIN32

pdf_document::pdf_document(const wchar_t *pdfFileName)
{
    load(pdfFileName);
}


bool pdf_document::load(const wchar_t *pdfFileName)
{
    if (pdfFileName == nullptr) {
        return false;
    }

    size_t len = wcslen(pdfFileName);
    wchar_t *locFileName = new wchar_t[len + 1];
    wcscpy(locFileName, pdfFileName);
    std::unique_lock<std::mutex> locker(m_mutex);
    m_pdf_doc.reset(new PDFDoc(locFileName, static_cast<int>(len)));
    return m_pdf_doc->isOk();
}

#else

pdf_document::pdf_document(const char *pdfFileName)
{
    loadPdfFile(pdfFileName);
}


bool pdf_document::load(const char *pdfFileName)
{
    if (pdfFileName == nullptr) {
        return false;
    }
    GString *fileName = new GString(pdfFileName);
    std::unique_lock<std::mutex> locker(m_mutex);
    m_pdf_doc.reset(new PDFDoc(fileName));
    return m_pdf_doc->isOk();
}

#endif




bool pdf_document::loaded() const
{
    return m_pdf_doc != nullptr && m_pdf_doc->isOk();
}

std::shared_ptr<page_output_dev> pdf_document::get_page_geometry(int page, int dpi)
{
    std::unique_lock<std::mutex> locker(m_mutex);
    if (!loaded()) {
        throw std::logic_error("Document not loaded");
    }
    if (page < 1 || page > m_pdf_doc->getNumPages()) {
        throw std::invalid_argument("Invalid page number");
    }
    std::shared_ptr<page_output_dev> result_device = std::make_shared<page_output_dev>();
    m_pdf_doc->displayPage(result_device.get(), page, dpi, dpi, 0, gTrue, gTrue, gFalse);
    return result_device;
}



int pdf_document::pages_count() const
{
    if (m_pdf_doc == nullptr) {
        return 0;
    }
    return m_pdf_doc->getNumPages();
}
