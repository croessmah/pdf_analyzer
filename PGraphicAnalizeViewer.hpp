#ifndef PGRAPHICANALIZEVIEWER_HPP
#define PGRAPHICANALIZEVIEWER_HPP

#include <QGraphicsView>
#include <QObject>

#include <list>
#include <memory>

#include "PGraphicsGraph.hpp"
#include "PGraphicsLinesStrip.hpp"
#include "PGraphicsText.hpp"
#include "CoordinatePage.hpp"
#include "optional.hpp"

class QMouseEvent;
class QPointF;
class QGraphicsLineItem;
class QGraphicsScene;
class QGraphicsRectItem;
class QGraphicsTextItem;
class PGraphicsGraph;
class PGraphicsLinesStrip;


class PGraphicAnalizeViewer
    : public QGraphicsView
{
    Q_OBJECT
public:
    enum class State
    {
        StartState,
        FindFirstPoint,
        FindSecondPoint,
        GraphicsAnalizing,
    };

    PGraphicAnalizeViewer(QWidget *parent = nullptr);
    bool setCoordinatePage(const CoordinatePage &cp);
    void addGraphLines(PGraphicsGraph *graph);
    bool isAllHidden() const;
signals:
    void rectChoosed(QRectF rect, std::list<PGraphicsGraph*> gr, std::list<PGraphicsText*> texts);
public slots:
    void stateAreaMode();
    void stateStartMode();
protected:
    void resizeEvent(QResizeEvent *event);

    // QWidget interface
protected:
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
private:
    void analizeAndEmit();
    optional<QPointF> findMinDistancePoint(const QPointF &);
    optional<QPointF> findMinDistancePointFromRect(const QPointF &, const QRectF &);
    QGraphicsScene *mScene;
    QGraphicsLineItem *mLineVis;
    QGraphicsRectItem *mRectVis;
    QRectF mSceneViewRect;
    State mState;
    optional<QPointF> mCurrentPoint;
    optional<QPointF> mFirstPoint;
    optional<QPointF> mSecondPoint;
    std::list<PGraphicsGraph*> mGraphics;
    std::list<PGraphicsLinesStrip*> mOtherLines;
    std::list<QGraphicsTextItem*> mTexts;

};

#endif // PGRAPHICANALIZEVIEWER_HPP
