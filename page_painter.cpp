#include "page_painter.hpp"
#include "page_output_dev.hpp"
#include "real_tools.hpp"
#include <QPainter>
#include <QPen>
#include <QFont>
#include <QString>

page_painter::page_painter()
{
}

page_painter::page_painter(page_output_dev &dev)
{
    paint(dev);
}

void page_painter::paint(page_output_dev &dev)
{
    auto &pathes = dev.pathes();
    auto &words = dev.words();
    std::unique_ptr<QPixmap> pixmap =
        std::make_unique<QPixmap>(dev.get_page_rect().right(), dev.get_page_rect().bottom());
    pixmap->fill(Qt::white);
    QPainter painter(pixmap.get());

    QPen line_pen(Qt::SolidLine);
    for (auto &path: pathes) {
        if (path.empty()) {
            continue;
        }
        //todo: стиль линий
        line_pen.setWidthF(path.width());
        painter.setPen(line_pen);
        for (size_t i = 1; i < path.size(); ++i) {
            painter.drawLine(path[i - 1], path[i]);
        }
    }
    QFont font;
    font.setFamily(font.defaultFamily());    
    for (auto &word: words) {
        font.setItalic(word.italic);
        font.setBold(word.bold);
        font.setPointSizeF(word.fontSize * 0.75);
        painter.setFont(font);
        painter.drawText(word.cCoord, word.str);
    }
    m_pixmap = std::move(pixmap);
}

const QPixmap &page_painter::pixmap() const noexcept
{
    if (m_pixmap) {
        return *m_pixmap;
    }
    throw std::logic_error("null pixmap");
}
