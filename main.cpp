#include <QApplication>
#include <QLocale>

#include "GlobalParams.h"
#include "gui_document_previewer.hpp"
#include <list>
#include <QDebug> //todo:: delete this
#include <QString> //and this
#include <QHBoxLayout> //and this
#include <QStyleFactory> //and this




#include "gui_global_resource.hpp"
#include "pdf_document.hpp"
#include "gui_document_previewer.hpp"
#include "gui_page_preview_list.hpp"
#include "gui_page_preview_item.hpp"
#include "page_painter.hpp"
#include "gui_page_analize_viewer.hpp"

#include <QLabel>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::setStyle("Fusion");
    QLocale locale(QLocale::Language::English);
    QLocale::setDefault(locale);
    qDebug() << get_gres_path();
    globalParams = new GlobalParams(nullptr);

    QWidget lbl;
    QHBoxLayout *layout = new QHBoxLayout();
    gui_document_previewer *doc = new gui_document_previewer(&lbl);
    gui_page_analize_viewer *page = new gui_page_analize_viewer(&lbl);
    QObject::connect(doc, &gui_document_previewer::page_choosed, page, &gui_page_analize_viewer::load_geometry);
    doc->setMaximumWidth(230);
    //doc->setMinimumWidth(230);
    layout->addWidget(doc);
    layout->addWidget(page);
    layout->setMargin(0);
    layout->setSpacing(0);

    lbl.setLayout(layout);
    lbl.show();
    int resultValue = a.exec();

    return resultValue;
}
