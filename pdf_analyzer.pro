#-------------------------------------------------
#
# Project created by QtCreator 2017-02-11T22:52:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pdf_analyzer
TEMPLATE = app
CONFIG += c++14

#QMAKE_CXXFLAGS += -std=c++14


win32 {
   XPDFPATH = "$$PWD/xpdf-dep/win"
} else {
   XPDFPATH = "$$PWD/xpdf-dep/linux"
}

INCLUDEPATH += "$$XPDFPATH/xpdf_d"
INCLUDEPATH += "$$XPDFPATH/xpdf_d/fofi"
INCLUDEPATH += "$$XPDFPATH/xpdf_d/goo"
INCLUDEPATH += "$$XPDFPATH/xpdf_d/splash"
INCLUDEPATH += "$$XPDFPATH/xpdf_d/xpdf"
LIBS += -L"$$XPDFPATH/xpdf_d" -lxpdf_d
#LIBS += -L"$$XPDFPATH/xpdf_d/xpdf" -lxpdf
#LIBS += -L"$$XPDFPATH/xpdf_d/splash" -lsplash
#LIBS += -L"$$XPDFPATH/xpdf_d/fofi" -lfofi
#LIBS += -L"$$XPDFPATH/xpdf_d/goo" -lgoo
DEFINES += DEMO



SOURCES += main.cpp\
    GlobalSettings.cpp\
    points_path.cpp \
    line_segment.cpp \
    graphic_grid.cpp \
    graphics_box.cpp \
    graphics_box_builder.cpp \
    graphic.cpp \
    gui_page_analize_viewer.cpp \
    gui_global_resource.cpp \
    gui_arrow_button.cpp \
    gui_collapse_widget.cpp \
    page_output_dev.cpp \
    gui_page_preview_item.cpp \
    gui_page_preview_list.cpp \
    page_painter.cpp \
    pdf_document.cpp \
    document_loader.cpp \
    gui_document_previewer.cpp


HEADERS  += GlobalSettings.hpp\
    line_segment.hpp \
    points_path.hpp \
    graphic_grid.hpp \
    graphics_box.hpp \
    graphics_box_builder.hpp \
    real_tools.hpp \
    optional.hpp \
    graphic.hpp \
    pointers.hpp \
    gui_page_analize_viewer.hpp \
    gui_global_resource.hpp \
    gui_arrow_button.hpp \
    gui_collapse_widget.hpp \
    page_output_dev.hpp \
    gui_page_preview_item.hpp \
    gui_page_preview_list.hpp \
    page_painter.hpp \
    pdf_document.hpp \
    document_loader.hpp \
    gui_document_previewer.hpp
