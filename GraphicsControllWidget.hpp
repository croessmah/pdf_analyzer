#ifndef SCALEDTEXTWIDGET_HPP
#define SCALEDTEXTWIDGET_HPP

#include <QObject>
#include <QWidget>


class QPointF;
class QLineEdit;
class QComboBox;

class GraphicsControllWidget : public QWidget
{
    Q_OBJECT
public:
    explicit GraphicsControllWidget(QWidget *parent = 0);
    int getDataType() const;
    QString getYXType() const;
signals:
    void changed(const QPointF &nullPoint, const QPointF &maxPoint);
public slots:
    void reset();
    void updateValues();
    void setInvalidDataType();
    void dataTypeChanged(int index);
private:
    QLineEdit *mNullX;
    QLineEdit *mNullY;
    QLineEdit *mMaxX;
    QLineEdit *mMaxY;
    QComboBox *mDataType;
    QComboBox *mYXNames;
};

#endif // SCALEDTEXTWIDGET_HPP
