#include "gui_collapse_widget.hpp"
#include "gui_global_resource.hpp"
#include <QPushButton>
#include <QPixmap>
#include <QBrush>
#include <QVBoxLayout>

//todo: delete
#include <QLabel>


gui_collapse_widget::gui_collapse_widget(directions dir, QWidget *some_widget, QWidget *parent):
    QWidget(parent)
{
    gui_arrow_button::directions arrow_direction = gui_arrow_button::directions::ToRight;
    m_dir = dir;
    if (dir == directions::LeftToRight) {
        arrow_direction = gui_arrow_button::directions::ToLeft;
    } else {
        arrow_direction = gui_arrow_button::directions::ToRight;
    }
    setup(arrow_direction, some_widget);

}

QWidget *gui_collapse_widget::get_subwidget() const noexcept
{
    return m_some_widget;
}




void gui_collapse_widget::hide_panel(bool value)
{
    m_some_widget->setHidden(value);
    //todo: vertical
    Qt::Alignment align;
    if (m_dir == directions::LeftToRight) {
        align = value? Qt::AlignRight: Qt::AlignLeft;
    } else {
        align = value? Qt::AlignLeft: Qt::AlignRight;
    }
    layout()->setAlignment(m_hide_button, align);
    emit collapse(value);
}

void gui_collapse_widget::setup(gui_arrow_button::directions dir, QWidget *some_widget)
{
    //todo: vertical
    m_some_widget = some_widget;
    m_hide_button = new gui_arrow_button(dir, this);
    if (dir == gui_arrow_button::directions::ToLeft || dir == gui_arrow_button::directions::ToRight) {
        m_hide_button->setMaximumWidth(m_hide_button->get_size().width());
        m_hide_button->setMinimumWidth(m_hide_button->get_size().width());
    } else {
        m_hide_button->setMaximumHeight(m_hide_button->get_size().height());
        m_hide_button->setMinimumHeight(m_hide_button->get_size().height());
    }

    QLayout *main_layout = nullptr;
    switch(dir)
    {
    case gui_arrow_button::directions::ToLeft:
        main_layout = new QHBoxLayout;
        main_layout->addWidget(m_hide_button);
        main_layout->addWidget(some_widget);
        break;
    case gui_arrow_button::directions::ToRight:
        main_layout = new QHBoxLayout;
        main_layout->addWidget(some_widget);
        main_layout->addWidget(m_hide_button);
        break;
    case gui_arrow_button::directions::ToTop:
        main_layout = new QVBoxLayout;
        break;
    case gui_arrow_button::directions::ToBottom:
        main_layout = new QVBoxLayout;
        break;
    }
    main_layout->setMargin(0);
   // main_layout->setAlignment(m_some_widget, Qt::ALi);
    setLayout(main_layout);
    connect(m_hide_button, &gui_arrow_button::toggled, this, &gui_collapse_widget::hide_panel);
    hide_panel(m_hide_button->get_state());
}
