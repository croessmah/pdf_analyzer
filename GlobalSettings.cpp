#include "GlobalSettings.hpp"

GlobalSettings::GlobalSettings()
{
    reset();
}

void GlobalSettings::reset()
{
    setRecommendLineWidth(1.307);
    setRecommendLineWidthEps(0.05);
    setNonRecommendLineWidth(0.455);
    setNonRecommendLineWidthEps(0.05);
    setPointEps(0.05);
    setPageClipLeft(50);
    setPageClipTop(50);
    setPageClipRight(830);
    setPageClipBottom(1050);
    //setFoundedWidth({});
}

double GlobalSettings::getRecommendLineWidth() const noexcept
{
    return mRecommendLineWidth;
}

void GlobalSettings::setRecommendLineWidth(double value) noexcept
{
    mRecommendLineWidth = value;
}

double GlobalSettings::getRecommendLineWidthEps() const noexcept
{
    return mRecommendLineWidthEsp;
}

void GlobalSettings::setRecommendLineWidthEps(double value) noexcept
{
    mRecommendLineWidthEsp = value;
}

double GlobalSettings::getNonRecommendLineWidth() const noexcept
{
    return mNonRecommendLineWidth;
}

void GlobalSettings::setNonRecommendLineWidth(double value) noexcept
{
    mNonRecommendLineWidth = value;
}

double GlobalSettings::getNonRecommendLineWidthEps() const noexcept
{
    return mNonRecommendLineWidthEsp;
}

void GlobalSettings::setNonRecommendLineWidthEps(double value) noexcept
{
    mNonRecommendLineWidthEsp = value;
}

double GlobalSettings::getPointEps() const noexcept
{
    return mPointEps;
}

void GlobalSettings::setPointEps(double value) noexcept
{
    mPointEps = value;
}

double GlobalSettings::getPageClipLeft() const noexcept
{
    return mPageClipLeft;
}

void GlobalSettings::setPageClipLeft(double value) noexcept
{
    mPageClipLeft = value;
}

double GlobalSettings::getPageClipTop() const noexcept
{
    return mPageClipTop;
}

void GlobalSettings::setPageClipTop(double value) noexcept
{
    mPageClipTop = value;
}

double GlobalSettings::getPageClipRight() const noexcept
{
    return mPageClipRigth;
}

void GlobalSettings::setPageClipRight(double value) noexcept
{
    mPageClipRigth = value;
}

double GlobalSettings::getPageClipBottom() const noexcept
{
    return mPageClipBottom;
}

void GlobalSettings::setPageClipBottom(double value) noexcept
{
    mPageClipBottom = value;
}

const std::vector<double> &GlobalSettings::getFoundedWidth() const noexcept
{
    return mFoundedWidth;
}

void GlobalSettings::setFoundedWidth(const std::vector<double> &values)
{
    mFoundedWidth = values;
}



GlobalSettings &gSettings()
{
    static GlobalSettings settings;
    return settings;
}
