#ifndef PDF_ANALYZER_COORDINATEPAGE_HPP
#define PDF_ANALYZER_COORDINATEPAGE_HPP

#include <page_output_dev.hpp>
#include <memory>

class PDFDoc;

class CoordinatePage
{
public:
    using WordData = page_output_dev::word;
    CoordinatePage(PDFDoc *pdfDoc, int page);
    double getPageWidth() const;
    double getPageHeight() const;
    std::list<WordData> const &getWordsData() const;
    std::list<points_path> const &getPathesList() const;
    int getPageNum() const;
private:
    double mCropLeft;
    double mCropTop;
    double mCropRight;
    double mCropBottom;
    double mWidth;
    double mHeight;
    std::unique_ptr<page_output_dev> mOutputDev;
    int mPageNum;
};

#endif // PDF_ANALYZER_COORDINATEPAGE_HPP
