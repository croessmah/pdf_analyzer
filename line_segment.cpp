#include "line_segment.hpp"
#include "real_tools.hpp"
#include <array>
#include <algorithm>

#include <QPointF>

line_segment::line_segment(const QLineF &line, double width, int style)
    : m_line(line), m_width(width), m_style(style)
{
}


bool line_segment::try_add(const line_segment &line, qreal width_eps, qreal point_eps) noexcept
{
    if (qreal_compare(m_width, line.m_width, width_eps)) {

        std::array<QPointF, 4> array_of_points
        {{
                m_line.p1(), m_line.p2(),
                line.p1(), line.p2()
        }};

        auto point_less_comparator = [point_eps] (const QPointF &f, const QPointF&s) {
            return point_lex_less_compare(f, s, point_eps);
        };
        auto point_comparator = [point_eps] (const QPointF &f, const QPointF&s) {
            return point_compare(f, s, point_eps);
        };

        std::sort(array_of_points.begin(), array_of_points.end(), point_less_comparator);
        auto equal_find = std::adjacent_find(array_of_points.begin(), array_of_points.end(), point_comparator);
        if (equal_find != array_of_points.end()) {
            std::rotate(array_of_points.begin(), equal_find, array_of_points.end());
            QLineF new_line(array_of_points[2], array_of_points[3]);
            if (qreal_less_compare(m_line.length(), new_line.length(), point_eps)) {
                m_line = new_line;
                return true;
            }
        }
    }
    return false;
}

bool line_segment::is_vertical(qreal point_eps) const noexcept
{
    return qreal_compare(m_line.p1().x(),  m_line.p2().x(), point_eps);
}

bool line_segment::is_horizontal(qreal point_eps) const noexcept
{
    return qreal_compare(m_line.p1().y(),  m_line.p2().y(), point_eps);
}

bool line_segment::is_point(qreal point_eps) const noexcept
{
    return point_compare(m_line.p1(), m_line.p2(), point_eps);
}

void line_segment::line_left_to_right() noexcept
{
    if (m_line.p2().x() < m_line.p1().x()) {
        QPointF n_p1 = m_line.p2();
        QPointF n_p2 = m_line.p1();
        m_line.setPoints(n_p1, n_p2);
    }
}

qreal line_segment::length() const noexcept
{
    return m_line.length();
}

qreal line_segment::square_length() const noexcept
{
    qreal dx = x2() - x1();
    qreal dy = y2() - y1();
    return dx * dx + dy * dy;
}

int line_segment::style() const noexcept
{
    return m_style;
}

void line_segment::style(int s) noexcept
{
    m_style = s;
}

const QLineF &line_segment::line() const noexcept
{
    return m_line;
}

QPointF line_segment::p1() const noexcept
{
    return m_line.p1();
}

QPointF line_segment::p2() const noexcept
{
    return m_line.p2();
}

void line_segment::p1(const QPointF &p) noexcept
{
    m_line.setP1(p);
}


void line_segment::p2(const QPointF &p) noexcept
{
    m_line.setP2(p);
}


qreal line_segment::x1() const noexcept
{
    return m_line.x1();
}

qreal line_segment::y1() const noexcept
{
    return m_line.y1();
}


qreal line_segment::x2() const noexcept
{
    return m_line.x2();
}


qreal line_segment::y2() const noexcept
{
    return m_line.y2();
}





double line_segment::width() const noexcept
{
    return m_width;
}

void line_segment::width(double line_width) noexcept
{
    m_width = line_width;
}
