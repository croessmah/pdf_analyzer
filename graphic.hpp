#ifndef PDF_ANALYZER_GRAPHIC_HPP
#define PDF_ANALYZER_GRAPHIC_HPP

#include "graphics_box.hpp"
#include "graphic_grid.hpp"
#include "points_path.hpp"
#include "optional.hpp"

#include <memory>
/*
class graphic
{
public:
    graphic(std::shared_ptr<graphics_box> box, std::shared_ptr<graphic_grid> grid = nullptr);
    void push_back(std::shared_ptr<points_path> path);
    void push_front(std::shared_ptr<points_path> path);
    std::shared_ptr<points_path> path(std::size_t index) const noexcept;
    optional<qreal> get_point_by_x() const noexcept;
    optional<qreal> get_point_by_y() const noexcept;
private:
    std::shared_ptr<graphics_box> m_box;
    std::shared_ptr<graphic_grid> m_grid;
    std::list<std::shared_ptr<points_path>> m_pathes;
    static std::shared_ptr<graphic_grid> default_grid;
};
*/
#endif // PDF_ANALYZER_GRAPHIC_HPP
