#include <QLineEdit>
#include <QDoubleValidator>
#include <QPoint>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QString>
#include <QComboBox>

#include "GraphicsControllWidget.hpp"

GraphicsControllWidget::GraphicsControllWidget(QWidget *parent) : QWidget(parent)
{
    mNullX = new QLineEdit(this);
    mNullY = new QLineEdit(this);
    mMaxX = new QLineEdit(this);
    mMaxY = new QLineEdit(this);

    mDataType = new QComboBox(this);
    mDataType->addItem(tr("Не установлено"), QVariant(0));
    mDataType->addItem(tr("Данные насосов"), QVariant(1));
    mDataType->addItem(tr("Другие данные"), QVariant(2));


    mYXNames = new QComboBox(this);
    mYXNames->addItem(tr("Не установлено"), QVariant(QString("")));
    mYXNames->addItem(tr("H / Q"), QVariant(QString("H/Q")));
    mYXNames->addItem(tr("P1 / Q"), QVariant(QString("P1/Q")));
    mYXNames->addItem(tr("P2 / Q"), QVariant(QString("P2/Q")));
    mYXNames->addItem(tr("Eta / Q"), QVariant(QString("Eta/Q")));
    //todo: load from config


    mNullX->setValidator(new QDoubleValidator());
    mNullY->setValidator(new QDoubleValidator());
    mMaxX->setValidator(new QDoubleValidator());
    mMaxY->setValidator(new QDoubleValidator());

    QHBoxLayout *xLineLayout = new QHBoxLayout;
    xLineLayout->addWidget(new QLabel(tr("Шкала X:")));
    xLineLayout->addWidget(mNullX);
    xLineLayout->addWidget(new QLabel("-"));
    xLineLayout->addWidget(mMaxX);
    QHBoxLayout *yLineLayout = new QHBoxLayout;
    yLineLayout->addWidget(new QLabel(tr("Шкала Y:")));
    yLineLayout->addWidget(mNullY);
    yLineLayout->addWidget(new QLabel("-"));
    yLineLayout->addWidget(mMaxY);

    QPushButton *buttonReset = new QPushButton(tr("Сброс"));
    QHBoxLayout *buttonsLayout = new QHBoxLayout;

    buttonsLayout->addWidget(mDataType);
    buttonsLayout->addWidget(mYXNames);
    buttonsLayout->addWidget(buttonReset);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addLayout(xLineLayout);
    mainLayout->addLayout(yLineLayout);
    mainLayout->addLayout(buttonsLayout);
    this->setLayout(mainLayout);

    connect(buttonReset, SIGNAL(clicked()), SLOT(reset()));
    connect(mNullX, SIGNAL(editingFinished()), SLOT(updateValues()));
    connect(mNullY, SIGNAL(editingFinished()), SLOT(updateValues()));
    connect(mMaxX, SIGNAL(editingFinished()), SLOT(updateValues()));
    connect(mMaxY, SIGNAL(editingFinished()), SLOT(updateValues()));
    connect(mDataType, SIGNAL(activated(int)), SLOT(dataTypeChanged(int)));
    reset();
}


int GraphicsControllWidget::getDataType() const
{
    return mDataType->currentData().toInt();
}

QString GraphicsControllWidget::getYXType() const
{
    return mYXNames->currentData().toString();
}


void GraphicsControllWidget::reset()
{
    mNullX->setText("0.0");
    mNullY->setText("0.0");
    mMaxX->setText("1.0");
    mMaxY->setText("1.0");
    setInvalidDataType();
    emit changed(QPointF(0.0, 0.0), QPointF(1.0, 1.0));
}

void GraphicsControllWidget::updateValues()
{
    bool isOk[4] = {false};
    double nullX = mNullX->text().replace(',', '.').toDouble(isOk);
    double nullY = mNullY->text().replace(',', '.').toDouble(isOk + 1);
    double maxX = mMaxX->text().replace(',', '.').toDouble(isOk + 2);
    double maxY = mMaxY->text().replace(',', '.').toDouble(isOk + 3);
    for (bool ok: isOk) {
        if (!ok) {
            reset();
            return;
        }
    }
    emit changed(QPointF(nullX, nullY), QPointF(maxX, maxY));
}

void GraphicsControllWidget::setInvalidDataType()
{
    mDataType->setCurrentIndex(0);
}

void GraphicsControllWidget::dataTypeChanged(int index)
{
    switch (index)
    {
    case 0:
        mYXNames->setCurrentIndex(0);
        break;
    case 1:
        mYXNames->setCurrentIndex(1);
        break;
    case 2:
        mYXNames->setCurrentIndex(0);
        break;
    default:
        break;
    }
}
