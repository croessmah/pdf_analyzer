#ifndef PAGE_OUTPUT_DEV_HPP
#define PAGE_OUTPUT_DEV_HPP

#include <list>
#include <array>
#include "points_path.hpp"

//#pragma GCC diagnostic ignored "-Wunused-parameter"
//#pragma GCC diagnostic warning "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "OutputDevFix.hpp"
#pragma GCC diagnostic warning "-Wunused-parameter"

#include <QString>
#include <QPointF>
#include <QRectF>

class page_output_dev
    : public OutputDev
{
public:
    struct word
    {
        QPointF cCoord;
        QPointF dCoord;
        QPointF oCoord;
        QString str;
        QString fontName;
        double fontSize;
        bool bold;
        bool italic;
    };

    page_output_dev();
    void stroke(GfxState *state);
    void startPage(int pageNum, GfxState *state);
    const std::list<points_path> &pathes() const noexcept;
    const std::list<word> &words() const noexcept;
    const QRectF &get_page_rect() const noexcept;

    GBool upsideDown();
    GBool useDrawChar();
    GBool interpretType3Chars();
    GBool needCharCount();
    void incCharCount(int nChars);

    void drawChar(GfxState *state, double x, double y, double dx, double dy, double originX, double originY, CharCode code, int nBytes, Unicode *u, int uLen);
    void clip(GfxState *state);
    void eoClip(GfxState *state);
    void clipToStrokePath(GfxState *state);
private:
    int m_skip_char_count;
    void moveTo(const QPointF &point);
    void lineTo(const QPointF &point);
    void curveTo(const std::array<QPointF, 3> &points);
    std::list<word> m_words;
    std::list<points_path> m_pathes;
    QRectF m_page_rect;
    size_t m_curve_resolution;
};

#endif // PAGE_OUTPUT_DEV_HPP
